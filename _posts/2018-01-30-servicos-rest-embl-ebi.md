---
layout: review
title: "Serviços REST EMBL-EBI"
date: 2018-01-30 17:00:00 -0200
categories: 
about:
  - name: "EMBL-EBI, programmatically: an introduction"
    uri: "https://www.youtube.com/watch?v=roC_Ms_cgOs"
    authors:
      - name: "European Bioinformatics Institute - EMBL-EBI"
  - name: "EMBL-EBI Web Services - EMBL-EBI Web Services - EMBL-EBI Confluence"
    uri: "https://www.ebi.ac.uk/seqdb/confluence/display/WEBSERVICES"
    authors:
      - name: "European Bioinformatics Institute - EMBL-EBI"
---



## Interface RESTful

### Passos para execução:

![]({{ site.baseurl }}/assets/img/embl-ebi-restful-interaction-activity.svg)

### Recursos:

#### /parameters/
Retorna uma representação contendo os ids dos parâmetros usados durante uma submissão.

Exemplo de resposta:

```xml
<parameters>
	<id>parameter1_name</id>
	<id>parameter2_name</id>
	...
</parameters>
```

#### /parameterdetails/:parameterN_id
Retorna uma representação contendo uma descrição dos parâmetros.

Exemplo de resposta:

```xml
<parameter>
	<name>paramenterN_name</name>
	<description>A textual description...</description>
	<type>COMMAND</type>
	<values>
		<label>blastp</label>
		<value>blastp</value>
		<defaultValue>true</defaultValue>
		<properties>
			<property>......</property>
		</properties>
	</values>
<parameter>
```

#### /run/
Recebe uma requisição POST com os parâmetros de execução. Retorna um job ID.

```bash
curl --request POST \
  --url http://www.ebi.ac.uk/Tools/services/rest/iprscan5/run/ \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data 'email=ricardo.cawal%40gmail.com&title=teste&appl=ProDom&goterms=true&pathways=true&sequence=GGCAGATTCCCCCTAGACCCGCCCGCACCATGGTCAGGCATGCCCCTCCTCATCGCTGGGCACAGCCCAGAGGGT%0AATAAACAGTGCTGGAGGCTGGCGGGGCAGGCCAGCTGAGTCCTGAGCAGCAGCCCAGCGCAGCCACCGAGACACC%0AATGAGAGCCCTCACACTCCTCGCCCTATTGGCCCTGGCCGCACTTTGCATCGCTGGCCAGGCAGGTGAGTGCCCC%0ACACCTCCCCTCAGGCCGCATTGCAGTGGGGGCTGAGAGGAGGAAGCACCATGGCCCACCTCTTCTCACCCCTTTG%0AGCTGGCAGTCCCTTTGCAGTCTAACCACCTTGTTGCAGGCTCAATCCATTTGCCCCAGCTCTGCCCTTGCAGAGG%0AGAGAGGAGGGAAGAGCAAGCTGCCCGAGACGCAGGGGAAGGAGGATGAGGGCCCTGGGGATGAGCTGGGGTGAAC%0ACAGGCTCCCTTTCCTTTGCAGGTGCGAAGCCCAGCGGTGCAGAGTCCAGCAAAGGTGCAGGTATGAGGATGGACC%0ATGATGGGTTCCTGGACCCTCCCCTCTCACCCTGGTCCCTCAGTCTCATTCCCCCACTCCTGCCACCTCCTGTCTG%0AGCCATCAGGAAGGCCAGCCTGCTCCCCACCTGATCCTCCCAAACCCAGAGCCACCTGATGCCTGCCCCTCTGCTC%0ACACAGCCTTTGTGTCCAAGCAGGAGGGCAGCGAGGTAGTGAAGAGACCCAGGCGCTACCTGTATCAATGGCTGGG%0AGTGAGAGAAAAGGCAGAGCTGGGCCAAGGCCCTGCCTCTCCGGGATGGTCTGTGGGGGAGCTGCAGCAGGGAGTG%0AGCCTCTCTGGGTTGTGGTGGGGGTACAGGCAGCCTGCCCTGGTGGGCACCCTGGAGCCCCATGTGTAGGGAGAGG%0AAGGGATGGGCATTTTGCACGGGGGCTGATGCCACCACGTCGGGTGTCTCAGAGCCCCAGTCCCCTACCCGGATCC%0ACCTGGAGCCCAGGAGGGAGGTGTGTGAGCTCAATCCGGACTGTGACGAGTTGGCTGACCACATCGGCTTTCAGGA%0AGGCCTATCGGCGCTTCTACGGCCCGGTCTAGGGTGTCGCTCTGCTGGCCTGGCCGGCAACCCCAGTTCTGCTCCT%0ACTCCAGGCACCCTTCTTTCCTCTTCCCCTTGCCCTTGCCCTGACCTCCCAGCCCTATGGATGTGGGGTCCCCATC%0AATCCCAGCTGCTCCCAAATAAACTCCAGAAG'
```
```
iprscan5-R20180201-204435-0342-59606161-pg
```

#### /status/:job_id
Recebe uma requisição REST e retorna uma string com o estado do job. Valores possíveis:
- RUNNING: Em processamento;
- FINISHED: Terminado, resultados podem ser recuperados;
- ERROR: Um erro ocorreu durante a tentativa de recuperar o estado do trabalho;
- FAILURE: O trabalho acabou em falha;
- NOT_FOUND: O trabalho não foi encontrado.

Exemplo de requisição:

```bash
curl --request GET \
  --url http://www.ebi.ac.uk/Tools/services/rest/iprscan5/status/iprscan5-R20180201-204435-0342-59606161-pg
```
```
FINISHED
```


#### /resulttypes/:job_id
Retorna uma descrição dos diferentes resultados disponíveis para um dado trabalho. Alguns desses resultados são apenas outras representações de um resultado de interesse. Alguns podem mesmo retornar vazio.

Exemplo de requisição-resposta:

```bash
curl --request GET \
  --url http://www.ebi.ac.uk/Tools/services/rest/iprscan5/resulttypes/iprscan5-R20180201-204435-0342-59606161-pg
```

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<types>
	<type>
		<description>The output from the tool itself</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>out</identifier>
		<label>Tool Output</label>
		<mediaType>text/plain</mediaType>
	</type>
	<type>
		<description>The output from the tool itself</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>log</identifier>
		<label>Tool Output</label>
		<mediaType>text/plain</mediaType>
	</type>
	<type>
		<description>The results of the job in text format</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>tsv</identifier>
		<label>TSV Output</label>
		<mediaType>text/plain</mediaType>
	</type>
	<type>
		<description>The results of the job in XML</description>
		<fileSuffix>xml</fileSuffix>
		<identifier>xml</identifier>
		<label>XML Output</label>
		<mediaType>application/xml</mediaType>
	</type>
	<type>
		<description>The results of the job in a tarball zip file</description>
		<fileSuffix>html.tar.gz</fileSuffix>
		<identifier>htmltarball</identifier>
		<label>Tarball Output</label>
		<mediaType>application/gzip</mediaType>
	</type>
	<type>
		<description>The results of the job in GFF3 format</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>gff</identifier>
		<label>GFF Output</label>
		<mediaType>text/plain</mediaType>
	</type>
	<type>
		<description>The results of the job in a tarball zip file</description>
		<fileSuffix>svg</fileSuffix>
		<identifier>svg</identifier>
		<label>SVG Output</label>
		<mediaType>image/svg+xml</mediaType>
	</type>
	<type>
		<description>Your input sequence as seen by the tool</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>sequence</identifier>
		<label>Input Sequence</label>
		<mediaType>text/plain</mediaType>
	</type>
	<type>
		<description>The results of the job in JSON format</description>
		<fileSuffix>txt</fileSuffix>
		<identifier>json</identifier>
		<label>JSON Output</label>
		<mediaType>text/plain</mediaType>
	</type>
</types>
```


#### /results/:job_id/:type.identifier

Retorna a representação desejada do resultado.

```bash
curl --request GET \
  --url http://www.ebi.ac.uk/Tools/services/rest/iprscan5/result/iprscan5-R20180201-204435-0342-59606161-pg/xml
```
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<protein-matches
	xmlns="http://www.ebi.ac.uk/interpro/resources/schemas/interproscan5" interproscan-version="5.27-66.0">
	<protein>
		<sequence md5="d9b0dec4d486ff83c996cc48a57b7677">GGCAGATTCCCCCTAGACCCGCCCGCACCATGGTCAGGCATGCCCCTCCTCATCGCTGGGCACAGCCCAGAGGGTATAAACAGTGCTGGAGGCTGGCGGGGCAGGCCAGCTGAGTCCTGAGCAGCAGCCCAGCGCAGCCACCGAGACACCATGAGAGCCCTCACACTCCTCGCCCTATTGGCCCTGGCCGCACTTTGCATCGCTGGCCAGGCAGGTGAGTGCCCCCACCTCCCCTCAGGCCGCATTGCAGTGGGGGCTGAGAGGAGGAAGCACCATGGCCCACCTCTTCTCACCCCTTTGGCTGGCAGTCCCTTTGCAGTCTAACCACCTTGTTGCAGGCTCAATCCATTTGCCCCAGCTCTGCCCTTGCAGAGGGAGAGGAGGGAAGAGCAAGCTGCCCGAGACGCAGGGGAAGGAGGATGAGGGCCCTGGGGATGAGCTGGGGTGAACCAGGCTCCCTTTCCTTTGCAGGTGCGAAGCCCAGCGGTGCAGAGTCCAGCAAAGGTGCAGGTATGAGGATGGACCTGATGGGTTCCTGGACCCTCCCCTCTCACCCTGGTCCCTCAGTCTCATTCCCCCACTCCTGCCACCTCCTGTCTGGCCATCAGGAAGGCCAGCCTGCTCCCCACCTGATCCTCCCAAACCCAGAGCCACCTGATGCCTGCCCCTCTGCTCCACAGCCTTTGTGTCCAAGCAGGAGGGCAGCGAGGTAGTGAAGAGACCCAGGCGCTACCTGTATCAATGGCTGGGGTGAGAGAAAAGGCAGAGCTGGGCCAAGGCCCTGCCTCTCCGGGATGGTCTGTGGGGGAGCTGCAGCAGGGAGTGGCCTCTCTGGGTTGTGGTGGGGGTACAGGCAGCCTGCCCTGGTGGGCACCCTGGAGCCCCATGTGTAGGGAGAGGAGGGATGGGCATTTTGCACGGGGGCTGATGCCACCACGTCGGGTGTCTCAGAGCCCCAGTCCCCTACCCGGATCCCCTGGAGCCCAGGAGGGAGGTGTGTGAGCTCAATCCGGACTGTGACGAGTTGGCTGACCACATCGGCTTTCAGGAGGCCTATCGGCGCTTCTACGGCCCGGTCTAGGGTGTCGCTCTGCTGGCCTGGCCGGCAACCCCAGTTCTGCTCCTCTCCAGGCACCCTTCTTTCCTCTTCCCCTTGCCCTTGCCCTGACCTCCCAGCCCTATGGATGTGGGGTCCCCATCATCCCAGCTGCTCCCAAATAAACTCCAGAAG</sequence>
		<xref id="EMBOSS_001"/>
		<matches/>
	</protein>
</protein-matches>
``` 

#### /?wadl

Retorna uma descrição WADL do serviço. Exemplo: http://www.ebi.ac.uk/Tools/services/rest/iprscan5?wadl




## Clientes
- Serviços provêem clientes para serem usados/integrados na linha de comando.
- Serviços podem ser utilizado de forma síncrona e assíncrona segundo escolha do usuário aparentemente. Porém, isso parece uma escolha de uso do cliente, que se comporta de acordo com o pedido do usuário.
- Clientes fazem automaticamente o download de todos os arquivos de resultado.
	- Também permitem a escolha dos arquivos que são de interesse;
	- nome do arquivo é prefixado com ID do job;

## Níveis de erro
- **Cliente:** Erros de parâmetros passados aos clientes, não enviam os dados ao serviço;
- **Serviço:** Problemas de conectividade e validação dos dados, por exemplo. Dados são enviados ao serviço, porém atividade de análise não é executada;
- **Ferramenta:** Erro durante a execução da ferramenta.

## Outros detalhes
- Armazena os resultados por 7 dias;
- Limites de uso existem.


## Questões

Há um modelo de interação seguido de forma consistente no repositório?

> Sim. Serviços que possibilitam o acesso a bases de dados são diferentes, com única interação para recuperação da informação. Por sua vez, serviços que executam ferramentas seguem a seqüência de passos apresentada acima.

Segue as restrições RESTful?

1. Separação cliente-servidor/
2. comunicação stateless;
3. Respostas são cacheáveis;
4. Interface uniforme:
	1. identificação de recursos;
	2. manipulação de recursos por meio de representações;
	3. mensagens auto-descritivas;
	4. HATEOAS;

> 1, 2 e 3. Em 4.1, os recursos não são nomeados de maneira hierárquica. 4.2 é, de certa forma, seguido. 4.3 não é melhor seguido pois diferentes representações de um mesmo recurso estão em diferentes URIs. 4.4 não é seguido pois nenhum hyperlink é retornado.

Quais os formatos de representações em entradas/saídas?

> Form-encoded para a entrada. Diversos formatos para a saída. Formatos indicados na URI.

Nosso modelo de interação é generalizado o bastante para ser utilizado por esse repositório?

> Sim. Porém, o nosso exige mais interações para iniciar a execução de um trabalho: São necessárias 3 ou mais requisições para inicializar a atividade de análise e iniciar o trabalho no nosso modelo, enquanto é preciso apenas 1 requisição no modelo deles (caso o usuário já conheça os diversos parâmetros). 

O que esse repositório/serviço provê de interessante que poderia ser estudado em nosso modelo de interação?

> Descrição textual dos parâmetros utilizados.