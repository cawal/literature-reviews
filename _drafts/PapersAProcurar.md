
Rahon, D., Gayno, R., Gratien, J.-M., Le Fur, G., and
Schneider, S. (2012). Migration to model driven en-gineering in the development process of distributed
scientific application software. In Proceedings of the
3rd annual conference on Systems, programming, and
applications: software for humanity, SPLASH ’12,
pages 181–190, New York, NY, USA. ACM.


Bender, A., Poschlad, A., Bozic, S., and Kondov, I.
(2013). AService-oriented Framework for Integration
of Domain-specific Data Models in Scientific Work-flows. Procedia Computer Science, 18:1087 – 1096.
2013 International Conference on Computational Sci-


[21] R.V.V. Sanchez, R.R. de Oliveira, and R. Pontin de Mattos Fortes,
"RestML: Modeling RESTful Web Services", REST: Advanced
Research Topics and Practical Applications. Springer New York, 2014.

[22] M. Siikarla, M. Laitkorpi, P. Selonen, and T. Systä, "Transformations
have to be developed ReST assured", Theory and Practice of Model
Transformations , Springer Berlin Heidelberg, 2008.