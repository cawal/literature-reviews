# Eclipse Model Driven Technologies


## Acceleo (M2T)

**Framework para a transformação de modelos para texto**. Permitir que os arquivos criados possuam **seções protegidas**, as quais **mantém as alterações do usuário** mesmo que o código/texto seja gerado novamente.
https://wiki.eclipse.org/Acceleo/Getting_Started

## EMF Parsley (UI)

**Framework para o desenvolvimento rápidos de interfaces de usuário baseadas em modelos EMF.** Permite customização via injeção de dependências. Provê também **DSL** (baseada em Xtext) **para a configuração e customização** de componentes da interface. Permite também o **debug** da configuração gerada.
