Semantic model-driven architecting of service-based software systems
===================================================================

Resumo de [Pahl2007]:
> Pahl, Claus. 2007. “Semantic Model-Driven Architecting of Service-Based Software Systems.” Information and Software Technology 49 (8): 838–50. doi:10.1016/j.infsof.2006.09.007.

Apresentam uma transformação e técnicas de raciocínio  para a modelagem em camadas da arquitetura  de serviços semânticos baseada em ontologias.

Contribuem com um framework em camadas de transformação ontológica, com diferentes ontologias tendo foco nas necessidades da modelagem de serviços e precessos a uma camada de abstração partuicular.

____________________________________________________________


Introdução
----------

**Background:**
- Importância da modelagem como um meio de abstração + Necessidade de automatização => MDD como uma abordagem de desenvolvimento;
- Modelagem baseada em ontologias investigada como um framework de modelagem semântica para enriquecer as abordagens clássicas baseadas em UML;
  - Melhor suporte à reasoning e transformação => suporte à atividades de automatização;
  - Potencial para reuso maior do conhecimento formalizado como ontologias.
- Arquiteturas baseadas em serviços se tornaram um importante paradigma;
  - Necessidade de modelar e implementar esses sistemas orientados a serviços;
  - A modelagem e a descrição desses serviços reusáveis e  passíveis de composição é uma atividade central para provedores e clientes dos serviços;


**Foco do trabalho em WS baseados em:**
- WSDL;
- SOAP;
- também extensões em Web Service Business Process Execution Language (WS-BPEL).

Área útil para demonstrar os benefícios da modelagem semântica baseada em ontologias devido à sua orientação a componetes e natureza distribuída dos serviços, com ênfase na provisão e descoberta das descrições e no compartilhamento e reúso de modelos e serviços.



**Objetivo:**
Adaptar o desenvolvimento orientado a models para a arquitetura de serviços usando a tecnologias de ontologias como ferramenta dessa integração.


**Contribuições:**
- Propõe um framework para a arquitetura orientada a modelos baseado em ontologias de sistemas de software baseado em serviços.
  - Arquitetura orientada a modelos como abordagem de desenvolvimento, aplicada à arquitetura orientada a serviços como a plataforma e ontologias como técnica de modelagem semântica.
- Contribuem com um framework em camadas de transformação ontológica, com diferentes ontologias tendo foco nas necessidades da modelagem de serviços e precessos a uma camada de abstração particular.

*NOTA: ONTOLOGY DEFINITION METAMODEL (OMG) AINDA ESTAVA EM DESENVOLVIMENTO!*


___________________________________________________________

Desenvolvimento de arquiteturas de serviços
-------------------------------------------

- **Web Service:** sistema de software que provê um conjunto coerente de operações em uma dada localização.
  - O provedor do serviço produz uma descrição abstrata de sua interface e a disponibiliza;
  - Clientes em potencial utilizam dessa descrição para localizar e invocar essas operações usando mensagens.
  - Serviços existentes podem ser reutilizados e compostos de modo a processos de negócios e workflows;

A necessidade de tratar descrições semânticas abstratas e composição no contexto do desenvolvimento de software orientado a serviços faz da Model-Driven Architecture (MDA) em combinação com as ontologias um framework adequado.


**A plataforma do MDA, no contexto do trabalho, é baseada em serviços. I.e., a arquitetura genérica é  uma arquitetura orientada a serviços.** Fazem parte desta plataforma:
- A infraestrutura de descoberta e invocação do serviço;
- Os serviços;
- Os clientes dos serviços;


**Linguagens de Descrição são centrais em SOAs.** Porém, com a segunda geração de tecnologias para serviços e com tecnologias como o MDA, a ênfase se moveu da atividade de descrever o serviço para  a atividade de modelar o serviço.

**Linguagens para ontologias são técnicas para representação de conhecimento e inferência lógica.** Estas linguagens tornam possível criar um entendimento precisamente definido  e compartilhável das anotações de recursos/serviços.
- Esforços já utilizam as ontologias na engenharia de software em geral e na modelagem em particular.

- OWL-S: Ontologia de serviços. Provê um vocabulário específico para descrever propriedades e capacidades de serviços Web;
- OWL-DL: É equivalente à lógica de descrição, o que é particularmente interessante ao contexto de engenharia de software dado a correspondência entre lógica de descrição e lógica dinâmica (uma lógica modal de programas), utilizada para modelar e avaliar sistemas de software.


___________________________________________________________

Modelando com Ontologias
------------------------
> Essa seção apresenta um running example útil à compreensão

**MDA propõe 3 camadas de modelagem**. O paper vai buscar demonstrar que **todas essas camadas** podem ser **suportadas ontologicamente** para uma arquitetura orientada a modelos.

**Modelagem semântica (conceitual?) e ontologias são similares em propósito:**
- **Modelos:** usados na ES para especificar,  visualizar e documentar artefatos de software e suas propriedades. Podem ser usados para a realização de inferências usando um framework lógico.
- **Ontologias:** usadas como uma formulação de uma conceitulização de um domínio, estruturada em relação à hierarquia das entidades e conceitos desse domínio e suas propriedades e relacionamentos. Têm como propósito a classificação e a inferência.


### Computational Independent Models ###

CIMs são utilizados para capturar os conceitos e propriedades do domínio.

Tipicamente, 2 pontos-de-vista do domínio são utilizados:
- **Information Viewpoint:** captura os conceitos do domínio como hierarquias;
- **Enterprise/process viewpoint:** representa o comportamento de forma baseada em processos.

Um **3º ponto de vista é adicionado** pelo trabalho:
- **Structural viewpoint:** Trata das propriedades dos objetos e processos.

O trabalho busca, então, prover uma notação ontológica única que capture os três pontos de vista.

- **2 tipos de conceitos** são distinguidos:
  - objetos; e
  - processos.
- **3 tipos de relacionamentos** são distinguidos:
  - is a (subclasse);
  - has part; e
  - depends (útil para descrever relacionamentos de entradas e saídas entre objetos e processo).
- **Restrições**, ou **propriedades**, em conceitos e relacionamentos podem ser expressas utilizando fórmulas lógicas (permite deescrever requisitos de ordenações específicas quanto a processos compostos).


**Isso coloca os três viewpoints da seguinte maneira (Fig.1):**
- **Information Viewpoint:**  usa relacionamentos IS A;
- **Enterprise/process viewpoint:** usa relacionamentos DEPENDS.
- **Structural viewpoint:** usa relacionamentos HAS PART.



### Platform Independent Models ###

PIMs focam nas **restrições arquiteturais** impostas pelo ambiente computacional. **Arquiteturas e processos** são os aspectos chaves **nesse nível de modelagem de serviços**.
- Serviços;
- Configuração arquitetural desses serviços ;
- processos de interação (remote invocation and service activation)_;

O trabalho utiliza **ontologias para expressar estes aspectos**.

Serviços -> "Componentes" da SOA -> ponto de início de uma SOA.

Ontologias sobre serviços:
- Várias foram propostas;
- Diferem como serviços e processos são representados;
- Trabalho escolhe a WSPO

**Web Service Process Ontology (WSPO)**:
- Permite representar não apenas as propriedades de um serviço, mas também sua configuração e a sua construçãoem processos;
- Foca mais no aspecto arquitetural que frameworks de ontologias de serviço;
- Serviços e processos são representados como relações de acessibilidade entre estados de um sistema, formando um framework orientado a processos que permite o raciocínio modal sobre o comportamento do software;
- Distingüe-se das ontologias de serviços tradicionais porque:
	 - É baseada em lógica descritiva e permite expressão de processos baseada em iterações, composição sequencial e paralela e condicionais;
	 - Adiciona dados a um processo como parâmetros que são elementos constantes do processo.
	 (Como as outras fazem isso???) 


WSPO provê um template PIM para a descrição de serviços e de processos de um serviço.
- Estrutura básica de estados e processo de serviços;
- **3 conceitos ontológicos** da abordagem:
	- Estados
	- Parâmentos de entrada e saída;
	- Condições (pré- e pós-)
- **2 formas de relacionamentos** providas pela ontologia:
	- Trasitional relationships: *serviços e processos. (Mudam o estado do sistema?)*
	- Descriptional relationships: *Descriçoes sintáticas e semânticas. Usadas para associar objetos dos parâmetros (sintaxe) e condições (semântica).*
 

**Exemplos apresentados:**
- 3-A. Modelagem semântica do serviço: Um modelo semântico de cada serviço é criado, contendo aspectos sintáticos e semânticos. Restrições do CIM são incluídas como pré-condições.
- 3-B. Modelagem semântica do processo: Modelo PIM orientado a arquitetura e ao processo,focando nas aividades e em como elas são combinadas em processos.
- 4. Inferência: Inferência sobre a composição de serviços auxiliada pela WSPO; 


Embora a arquitetura seja o foco dos PSM, a abordagem não se qualifica como uma Architecture Description Language (ADL) porque:
- ADL usualmente permitem representar:
	- Componentes (aqui, serviços)
	- Conectores (canais entre serviços);
	- Configurações (montagens entre instâncias dos componentes e conectores);
- A abordagem representa:
	- Serviços como os Componentes;
	- Expressões de processos como as Configurações.
	
	
	
	
	
	### Platform-Specific Models ### 


A plataforma do trabalho é a **Plataforma de Serviços Web**.

Modelos da camada PSM precisam tratar de 2 aspectos:
- Um modelo da platafoma;
- Modelos específicos da implementação.

No trabalho:
- Modelo da plataforma: é restringido pelas tecnologias para Web Services e pelos princípios da SOA;
- Modelos específicos de implementação: caracterizam os modelos que suportam as linguagens predominantes na plataforma.

Plataforma de Web services é diferente de uma plataforma típica (CORBA, JEE...):
- Plataformas típicas: geração de programas executáveis é aspecto central;
- Plataforma de Web Services: É sobre...
	- Descrição sintática abstrata do serviço (WSDL);
	- Descrição semântica abstrata do serviço (WSMO e OWL-S);
	- Definição de processos de um serviço (WS-BPEL ou WS-CDL).
- Isso torna as transformações para essa camada diferentes dos mapeamentos tradicionais PIM2PSM (Como exatamente?)


O trabalho tem foco nos modelos para as linguagens da plataforma WSMO e WS-BPEL. Porém, um objetivo maior do MDD é prover transformações para uma grande variedade de linguagens alvo.

Interoperabilidade é foco da Web Services Platform. **2 interesses determinam as técnicas utilizadas nesse nível**: 
- Descrição abstrata do serviço;
- Associação padronizada de serviços a processos.

**Portanto, 2 modelos são relevantes** para prover suporte a linguagens executáveis e a linguagens suportadas por ferramentas:
- **Modelo de descrição e descoberta**: Interfaces sintáticas e semânticas abstratas do serviço, com foco em descrições aumentadas semanticamente no trabalho. OWL-S e WSMO;
- **Modelo de processos e composição**: Linguagens de coordenação de serviços. No trabalho, WS-BPEL, cujas especificações podem ser obtidas convertendo expressões de processos da WSPO.

Exemplos apresentados:
- 5A. **Descrição semântica do serviço** usando WSMO;
- 5B. **Definição do processo do serviço** usando WS-BPEL;





### Semântica das camadas ontológicas ###


Um metamodelo semântico para cada camada pode ser formulado:
- **CIM**: Uma **ontologia de domínio** pode ser definida em termos de *conjuntos* (para conceitos) e *relações* (para relacionamentos);
- **PIM**: **Aspectos arquiteturais e de processos** podem ser definidos em termos de *sistemas de transições rotuladas* (labelled transition systems);
- **PSM**: **Aspectos de interoperabilidade** podem ser divididos em *interface* (definido em termos de *conjuntos e relações*) e *comportamento de configuração e processo* (definido em termos de *mecanismos de transição*).




Transformações de modelos orientadas a ontologias
-------------------------------------------------


Para que uma abordagem em camadas seja proticável, transformações devem ser expolicitamente definidas.

Transformações entre camadas de modelagem devem ser automatizadas de modo a prover o suporte ferramental adequado e permitir o sucesso da abordagem.

No trabalho, não se busca o refinamento dos modelos. Informação não deve ser adicionada a um modelo em uma dada camada.Por outro lado, o trabalho também não visa uma automação completa, como presado pelo MDA. Ao invés disso, busca guiar o engenheiro de software
- **Mapeamento CIM-PIM:** Muda o foco da modelagem do domínio para a modelagem da aquitetura. Isso pediria informação adicional. Porém, um modelo detalhado contendo os três pontos de vista definidos  provê toda a informação necessitada para o template do modelo PIM.
- **Mapeamento PIM-PSM:** Requere informação adicional para a correta descrição abstrata de aspectos funcionais e nã-funcionais.



Exemplos mostrados:
- 6. Transformação de CIM para PIM.
- 7. Transformações de PIM para PSM.

**Apresentam as tabelas com as regras de transformação (1 e 2), uma figura ilustrando os mapeamentos entre os modelos utilizados (Fig 4) e uma figura detalhando as transformações entre as linguagens utilizadas (Fig 5).**

A definição formal das transformações é necessária para prover precisão à transformação em rempos de preservação semântica.



Modelagem - Padrões e Interoperabilidade
----------------------------------------

Interoperabilidade é uma questão central na MDA.

Linguagens para ontologias e UML se sobrepõe substancialmente e permitem conversões.

Perfis UML (como o ODM) permitem ainda mais interoperabilidade e, ainda, permitem o uso de sintaxe gráfica.

Padrões para a transformação também são importantes de serem considerados durante a definição da abordagem.


**Figura comparando MDA e MDA com ontologias (proposta do paper) (Fig 5)**.




Trabalhos relacionados
----------------------


Ontologias de serviços e frameworks:
- Framework WSMO: framework para a criação de ontologias de serviços;
- ontologia OWL-S: ;
- ontologia WSPO: composição e configuração arquitetural de serviços;
- ontologia FLOWS: semelhante a WSMO.

Modelagem arquitetural em camadas:
- Ontology Definition Metamodel;

Plataforma de Web Services:
- trabalhos conectando a polataforma a modelagem MDD/MDA/ES.



Conclusões
----------

O paper apresenta:
- um framework de modelagem e transformação semântico, integrado e em camadas para serviços.
- diferentes técnicas de modelagem orientadas a ontologias para prover suporte às aividades.

A plataforma final primária do framework são os serviços web.

Dado a necessidade de interoperabilidade e integração, é possível que problemas apareçam uma vez que MDA e a plataforma Web são desenvolvidas por organizações diferentes (OMG e W3C). Porém, iniciativas como o Ontology Definition Metamodel (ODM) tentam reconciliar os problemas que possam surgir.



