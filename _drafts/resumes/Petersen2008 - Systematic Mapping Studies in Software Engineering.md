MAPEAMENTO SISTEMÁTICO
======================

Resumo de [Petersen2008]:
> Petersen, Kai, Robert Feldt, Shahid Mujtaba, and Michael Mattsson. 2008. “Systematic Mapping Studies in Software Engineering.” EASE’08 Proceedings of the 12th International Conference on Evaluation and Assessment in Software Engineering, June. British Computer Society, 68–77. doi:10.1142/S0218194007003112.


Introdução
----------
Muitos resultados de pesquisas publicados
=> Necessidade de sumarizar e prover visão geral sobre uma dada área
=> Necessidade de metodologias específicas para os estudos secundários

Algumas áreas já possuem metodologias bem formadas.

Ainda havia pouco uso dessas metodologias em engenharia de software.








Revisão sistemática:
-----------------------------
- Método de estudo secundário
- Revisa a literatura primária em profundidade;
- descreve a metodologia e resultados de cada trabalho.

Benefícios:
- Metodologia bem definida para reduzir viés;
- O grande número de contextos permite conclusões mais gerais;
- Meta-análise estatística pode permitir conclusões que os estudos individuais não permitiriam.

Dificuldades:
- Esforço considerável para a sua execução








Mapeamento Sistemático
-----------------------------------
- Metodologia muito usada na pesquisa biomédica, mas menos usada em SE
<=? falta de um método para realizar a agregação da pesquisa em SE
- Recomendado  principalmente para áreas em que faltam estudos primários relevantes e de alta qualidade

Benefícios:
- Categoriza os relatórios e resultados de pesquisa encontrados de forma estruturada
- Geralmente provê uma sumarização visual (mapa) dos resultados;
- Requere menos esforço que a revisão sistemática

Porém/Vantagem...
- Provê uma visão mais grosseira da área de estudos que uma revisão sistemática

=> Uma forma interessante pode (?) ser:
Mapeamento sistemático de uma área -> Revisão Sistemática de um tópico







FIGURA 1 : PASSOS E RESULTADOS DO MAPEAMENTO SISTEMÁTICO

1- Definição da questão de pesquisa -> Escopo da revisão
2- Busca no escopo -> Todos os papers da área
3- Filtragem dos papers -> Papers relevantes
4- Palavras chaves a partir dos resumos -> Esquema de clçassificação
5- Extgração dos dados e processo de mapeamento -> Mapeamento Sistemático




1 - DEFINIÇÃO DO ESCOPO DE PESQUISA
==================================

O PRINCIPAL OBJETIVO de um mapemento sistemático é PROVER UMA VISÃO GERAL de uma ÁREA DE PESQUISA, identificando QUANTIDADE e TIPO das pesquisas e resultados da área.

Um objetivo secundário pode ser identificar onde as pesquisas na área são publicadas.

TABELA COM
- Nome do mapeamento (e.g. 'Object Oriented Design Map')
- Questões de pesquisas enumeradas (RQ1, RQ2,...)

As questões de pesquisa apresentadas são relacionadas a características da área de interesse...
- 'Quais os periódicos apresentam trabalhos da área?'
- 'Qual é o tópico mais investigado?'
- 'Qual o método de pesquisa mais utilizado?'
- 'Quais áreas dentro do escopo são estudadas em maior/menor frequencia?'
- Qual a novidade dentro da área?'




2 - CONDUÇÃO DA BUSCA POR ESTUDOS PRIMÁRIOS
=============================

Definir:
1. Bancos de dados científicos que serão consultados
2. Search strings (lógicas AND/OR/NOT) que serão usada para recuperar os resultados dos bancos.

Boa forma de criar uma search string: estruturar ele em termos  de população, intervenção, comparação e resultado dos trabalhos.

Palavras chaves das search strings podem ser obtidas de cada aspecto da estrutura.

Também pode ser feita manualmente, navegando por periódicos


Exemplo de search string:
Software Product Line Variability Map: ”software” AND (”product line” OR ”product family” OR ”system family”) AND (”variability” OR "variation")






3 - FILTRAGEM DE PAPERS (INCLUSÃO E EXCLUSÃO)
=============================================

- definir critérios de inclusão e exclusão para retirar papers que não respondem a questão do estudo
- construir TABELA detalhando esses critérios.

Apresenta exemplo interessante de tabelas de critérios.



4 - BUSCA DE PALAVRAS CHAVES (KEYWORDING) NOS RESUMOS
=====================================================

- Segue um processo sistemático (Fig 2)
- Keywording é uma forma de desenvolver rapidamente um esquema de classificação que leve em conta os estudos existentes;

Keywording em 2 passos:
1. Revisores lêem os resumos e buscam palavras chaves e conceitos que reflitam a contribuição do paper;
    - Também identificam o contexto da pesquisa;
2. Conjunto de palavras chaves dos diversos papers é combinado (UNION) para obter um entendimento de alto nível sobre a natureza e contribuição da pesquisa.
   - Isto auxilia na definição de um conjunto de categorias que seja representativo para a população de papers consultada

- Quando um conjunto final de palavras chaves é escolhido, elas podem ser clusterizadas e usadas para formar as categorias do mapa.
  - Um conjunto de CORTES/FACETAS (FACETS) PRINCIPAIS é, então, criado.
  -  Exemplos de facetas: tópico, tipo de contribuição, tipo/abordagem de pesquisa.
  - Uma boa escolha de facetas torna o processo de classificação mais fácil, de maneira que possa ser feito sem precisar olhar cada paper em detalhes.




  5 - EXTRAÇÃO DOS DADOS E MAPEAMENTO
 ===========================

 Com o esquema de classificação organizado, a extração real de dados começa.

 O esquema de classificação evolui dururante a extração:
 - MERGING E SPLITTING de categorias podem ocorrer durante a extração dos dados

 Uma TABELA é usada para DOCUMENTAR O PROCESSO DE EXTRAÇÃO de dados;

 Revisores proveêm razões breves para a acição de um paper a uma dada categoria.

 A tabela finalizada permite o CÁLCULO DE FREQUÊNCIAS de publicações em cada categoria.
 - Permite ver categorias enfatizadas no passado;
 - bem como categorias, gapes e piossibilidades para pesquisas futuras

 Essas frequencias podem ser apresentadas por visualizações úteis, como BUBBLE PLOT




 COMPARAÇÃO ENTRE MAPEAMENTOS E REVISÃO SISTEMÁTICA
 ==================================

 Objetivos:
 RS: Estabelecer o estados de evidências atual, identificar melhores práticas a partir de evidências empíricas
 MS: Classificação, análise temática e identificação de locais de publicação

 Processos:
 RS: Avaliação dos artigos quanto à qualidade, metanálise aprofundada e maior extração de dados
 MS: Análise temática, sem avaliação da qualidade dos artigos. Pode ser usado como início de uma RS.

 Largura e profundidade:
 RS: Maior profundidade, menor largura. Dá aos artigos um foco maior, oque aumenta a profundidade e, consequentemente, o esforço.
 MS: Maior largura, menor profundidade. Permite estruturar um campo de estudos maior.

 Classificação da área do tópico:
 RS: Restringe a amostra de papers, o que pode introduzir um viés em relação À percepção da área  quando focado em um método em particular.
 MS: Espera reduzir o viés de percepção da área.

 Classificação da abordagem de pesquisa:
 RS: Avalia em profudidade o artigo, com mais detalhes. Gera grande número de categorias possíveis para revisão.
 MS: Usa categorias amplas e de alto nível de abstração, de forma que não permite uma avaliação detalhada dos artiugos ao estruturar a área.

 Consideração de Validade:
 RS: minimiza a possibilidade de classificar errônaemente o estudo, mesmo que ele esteja errênamente apresentado no resumo.
 MS: Erros de classificação podem ocorrer  se as categorias são detalhadas e os papers estão erroneamente apresentados no resumo.

 Accessibilidadde à induústria e relevância:
 RS: Provê boas introduções para áreas específicas. Mas costuma ser de difícil entendimento inicialmente por serem muito detalhados.
 MS: Gera interesse pela facilidade de entendimento pela sua característica visual.


 GUIDELINES PARA RS E MS
 ================

 - Use os métodos complementarmente
 - Permita uma profundidade de leitura adaptável dado as necessidades de classificação
 - Classifique os paper em relação à evidência e novidade
 - Visualize os dados em gráficos
