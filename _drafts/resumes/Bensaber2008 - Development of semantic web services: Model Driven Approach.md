Development of semantic web services: Model Driven Approach
===========================================================

Resumo de [Bensaber2008]:
> Bensaber, Djamel Amar, and Mimoun Malki. 2008. “Development of Semantic Web Services: Model Driven Approach.” In Proceedings of the 8th International Conference on New Technologies in Distributed Systems (NOTERE), 307–17. New York, New York, USA: ACM Press. doi:10.1145/1416729.1416780.

Visão Geral do Trabalho
-----------------------

**GAP:** A pouca quantidade de ferramentas (em 2008) para criar especificações de serviços web semânticos é uma das barrerias ao seu maior uso.

**CONTRIBUIÇÃO:** O paper propõe uma abordagem orientada a modelos para a construção de especificações de serviços web semânticos em OWL-S.

**METODOLOGIA:**
1. Engenharia reversa de documentos WSDL em modelos de um dado perfil UML;
2. Anotação semântica do modelo UML com ontologias do domínio;
3. Ferramenta de conversão gera as descrições OWL-S a partir dos modelos UML criados.
___________________________________________________________
INTRODUÇÃO
----------

### Webservices: ###
- Tecnologia para o desenvolvimento e implantação de aplicações;
- Beneficiam-se de uma pilha bem estabelecida de padrões baseados em XML para o suporte à descrição, descoberta, interação e composição:
   - WSDL
   - UDDI
   - SOAP
   - BPEL4WS
   - ...


- Desafios aos WS:
  - Composição de serviços;
  - Segurança da descoberta;
  - Integração.


- A prática comum é especificar o acesso ao serviço com WSDL. Porém, essa linguagem não possui construtos semânticos e não é capaz de competir com esses desafios.

### Semantic Web Services: ###
- Extendem as capacidades de um WS associando conceitos semânticos a ele.
- Essa extensão visa proporcionar melhor busca, descoberta, seleção, composição e integração desses WS.
- Linguagens semanticamente enriquecidas (como OWL-S) foram criadas para descrever a semântica relacionada ao WS usando ontologias.


### Problema: ###
 A curva de aprendizagem dessas linguagens pode ser íngrime e é uma barreira ao seu maior uso.


### Model Driven Architecture:###
Abordagem de desenvolvimento orientado a modelos que faz uso de tecnologias OMG (UML, XMI, MOF,...).

UML pode ser uma plataforma de integração conveniente na modelagem de SWS, uma vez que modelos UML de SMSs possuem:
  - Expressividade: suficiente anotações semânticas e transformação completa de/para especificações de SWS;
  - Independência (da linguagem do SWS)
  - Legibilidade

### Proposta ####
O paper propõe uma abordagem para facilitar e elevar o grau de automação no processo de anotação semântica para WS.

### Abordagem: ###
- Usar conceitos e estratégias do MDA para separar o design da aplicação (lógica do negócio) da pĺataforma de implementação.
- Transformar UML/XMI em OWL-S/XML usando XSLT;
- Validar o exemplo usando a OWL-S API

___________________________________________________________


ABORDAGEM
---------

### 1 - Processo de engenharia reversa ###

**Input:** Arquivo WSDL
**Output:** Modelo UML (Diagrama de Classes e Diagramas de Atividades)

**Subatividades:**
1. **Interface Modeling:** especifica a interface dos serviços e suas operações;
2. **Workflow Modeling:** foca no comportamento interno de uma operação do WS, identificando as atividades e sua ordem para a realização da operação.

O **diagrama de classes** especifica a **interface** do serviço.

O **diagrama de atividades** são úteis para descrever o **processo de composição do modelo OWL-S**. Em sua maior parte, são obtidos automaticamente quando a ordem em que as atividades são executadas é expressa pelo desenvolvedor usando capacidades de data flow.

**APIS:**
- **JWSDL API:** Parsing do arquivo WSDL;
- **DOMSAX:** Manipular o conteúdo do XML Schema;

**Passos**
1. Arquivo WSDL é carregado e extraído usando JWSDL;
2. Uso da DOMSAX API para parsear o XML Schema;
3. Um conjunto de regras de transformação são aplicadas, gerando os elementos UML correspondentes;
4. Serialização do modelo UML em XMI.  




### 2 - Processo de anotação ###

**Input:** Diagramas de atividades gerados (skeleton UML profiles)
**Output:** Diagramas de atividades enriquecidos (completando as informaçẽos do perfil UML para SWS) por informações adicionadas por UDDI, ontologias e pelo desenvolvedor.

**UDDI:**
- contém informação útil para preencher a parte do perfil UML relacionado à perfis OWL-S (Nome da categoria, taxonomia...)
- A informação da categoria também auxilia em encontrar ontologias de domínio candidatas.

**Ontologias de domínio candidatas:**
- São importadas nos modelos UML segundo Djuric (2004);
- Ontologia é visualizada como uma interface UML e como diagramas UML;


**Execução:**
- Ontologias são importadas;
- Tipos complexos XSD apresentados como classes UML são mapeados para conceitos da ontologia;
- operações, parâmetros, pré- e pós-condições, bem como efeitos são linkados e anotados;
- Informação do UDDI é usada para preencher a categoria.
- Alguma informação adicional pode precisar ser provida.




### 3 - Ferramenta de Conversão ###

**Input:** Os modelos UML anotados obtidos no passo 2
**Output:** Documentos OWL-S descrevendo:
  - Service,
  - ServiceProfile,
  - ServiceModel; e
  - ServiceGrounding.

**XSLT:**
- Linguagem de escolha para a definição das regras de transformação;
- Transformações XSLT são executadas várias vezes para produzir os documentos OWL-S descrevendo:
  - Service,
  - ServiceProfile,
  - ServiceModel; e
  - ServiceGrounding.

### Comparação do processo com MDA ###

- **Criar um diagrama UML a partir de uma descrição WSDL:**
  PSM -> PIM (Engenharia reversa)
- **Transformação XSLT entre UML e OWL-S:**
  PIM -> PIM ('segundo nível' de PIM)
- **Especificação dos ServiceGroundings (concretização):**
  Criação de um PSM (provê informação específica da implementação do SWS).


__________________________________________________________

Perfil UML para SWS
-------------------

Usado para modelar diversos construtos OWL-S em conjunto com o diagrama de estrutura estática de UML.

**Metamodelos reutilizados no perfil**:
- **Ontology UML Profile (OUP, Djuric):** Relevante para a definição de associações no perfil, capturar conceitos das ontologias como propriedades e associações. Metaclasses importantes: *Ontology* e *OntClass*;
- **Metamodelo padrão de um modelo  de atividade UML 2.0**: Extendido para definir a metaclasse WebService e extensões para informação semântica.

<Muita informação de como o perfil é contruído e como a anotação de modelo é feita na página  309>

<Figura e tabela detalhando os elementos do perfil>


__________________________________________________________


Do WSDL para UML
----------------

- Definida em XSLT
- Deriva:
  - Diagrama de Classes: com as interfaces do serviço;
  - Diagrama de Atividades: especificando o comportamento interno da operação do serviço.
- Binding information não é incluída no diagrama de classes.
- Porém, binding information e URI de acesso são incluídas no diagrama de atividades para a transformação de UML para a concretização do serviço (groundings) no OWL-S.

[Resto da seção apresenta como esses diagramas são obtidos a partir de um exemplo]


_________________________________________________________

Transformações entre OWL-S e o perfil UML
-----------------------------------------


- Definida em XSLT

**Grounding:**
- Um ServiceGrouding consiste de um conjunto de AtomicProcessGroundings;
- Regras de transformação são apresentadas. Essencialmente:
- AtomicProcess corresponde a uma operação WSDL;
- Inputs e outputs são mapeados a messageParts correspondentes.
- Cada messagePart do WSDL pode ser especificadas em termos de parâmetros OWL-S.


**Validação:**
- Realizada com a OWL-S API (Java);
- Essa mesma API é usada para executar a especificação OWL-S gerada.

Figura 6 define uma visão esquemática da transformação;

Figuras 7 a 10 apresentam excertos de UML/XMI, XSLT, OWL-S e groundings.


__________________________________________________________

Trabalhos relacionados
----------------------

- **OWL-S Editor**: ferramenta proprietária edição visual de especificações OWL-S e modelagem do processo de composição como um diagrama de atividades UML padrão;

- **WSDL2OWLS (Paolucci et al, )**: Gera uma especificação OWL-S a partir de OWL-S. Gera um grounding completo e modelos incompletos de perfil e processo (?);

- **Shen, Yang, Zhu and Wan**: Especificação BPEL4WS é transformada em uma espeficicação OWL-S. Semelhante ao Paolucci (grounding completo e modelos incompletos de perfil e processo). Limitado a uma única instância de processo e de groundings;

- **Gronmo and al.**: Abordagem para a transformação de  OWL-S para diagramas de atividades UML e demonstra como as especificações completas do exemplo podem ser convertidas manualmente para diagramas de atividade UML.

- **Jaeger, Engel and Geihs**: Propõe metodologia para desenvolver descrições semânticas de WS usando OWL-S. Reconhecem a falta de ferramentas de suporte para o desenvolvimento dessas descrições semânticas. Um processo de três passos é provido para criar um template usando artefatos existentes (modelos, WSDL), automatizar a identificação de ontologias relevantes e realizar uma classificação baseada nessas ontologias.

- **Gannod and Timm**: Abordagem top-down que desenvolve especificações OWL-S de alto nível e usa a flexibilidade dos groundings OWL-S para mapear os serviços OWL-S para realizações WSDL potenciais de de um processo OWL-S.

- **Ontology Definition Metamodel (ODM-OMG)**: Perfil UML padrão para o suporte ao desenvolvimento ontológico via o ODM. O perfil  OWL do ODM provê suporte aos contrutos genéricos do OWL, mas não aos construtos específicos do OWL-S.

_________________________________________________________


Conclusão
---------

- OWL-S provê uma ontologia para WS que pode ser usado para descrever a semântica de um WS.
- Adotar uma linguagem como o OWL-S pode ser difícil dado a curva de aprendizado e a falta de ferramentas de suporte (2008);
- Gerar descrições de SWS a partir de modelos gráficos mais abstratos é um ganho, pois uma alternativa seria produzir essas descrições em baixo-nível usando XML;
- Umn problema de escalabilidade do projeto  apresentado era a anotação do perfil usando ontologias extensas. Esse problema poderia ser aliviado com uso de algoritmos de match;
- Também ainda não havia suporte a transformação de pré- e pós-condições;
- Uma pesquisa de interesse era a composição automática de serviços.
