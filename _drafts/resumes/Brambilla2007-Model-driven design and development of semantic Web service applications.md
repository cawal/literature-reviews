# Model-driven design and development of semantic Web service applications

Resumo de [Brambilla2007]:
> BRAMBILLA, M.; CERI, S.; FACCA, F. M.; et al. Model-driven design and development of semantic Web service applications. ACM Transactions on Internet Technology, v. 8, n. 1, p. 3–es, 1 nov 2007.

## Visão Geral

### Contribuição

- Propõe metodologia orientada a modelos para o design e desenvolvimento de aplicações de  serviços web semânticos e componentes dessas aplicações;
- **Foco no design e reengenharia de componentes de software de modo a cumprir os requisitos dos SWS**;
- Apresenta que **modelos de processos de negócio** e **modelos de engenharia Web** possuem expressividade suficiente para provêr suporte a extração semiautomática de descrições semânticas:
    - ontologias WSMO;
    - goals;
    - serviços web;
    - mediadores.
- Dessa maneira, esses modelos são capazes de abstrair parcialmente a complexidade em lidar com a semântica desses serviços.

### Métodos

- Baseado em:
    - **modelos BPMN** existentes criados para especificar os processos de negócio;
    - **especificações WebML** criadas como modelos para o design e desenvolvimento de aplicações de serviços web semânticos.

### Resultados

- A abordagem definida leva de uma visão abstrata das necessidades do negócio até uma implementação concreta da aplicação.
    - Uso de diversos passos;
    - Modelos de alto nível são transformados em componentes de software;
- Framework criado para melhorar a eficiência de todo o processo de design.


-----------------------------------------------------------

## Introdução

**Serviços web** são considerados uma **solução arquitetural** para a **modularização de software enterprise**.

- **Service Oriented Architecture (SOA)**
- Facilidade de integração dos serviços;


**Problema:** **Descoberta e integração** de serviços **não é automatica** e requer esforço humano.
**Possível solução:** **SemanticWeb**, com **descrições semânticas** dos recursos de modo a possibilitar a interação automática entre máquinas.

Diferentes propostas para Serviços Web Semânticos foram submetidos para a W3C:
- OWL-S
- WSMO
- WSDL-S
- ...

**Paper** foca no design e reeengenharia de software de modo a  cumprir os requisitos dos SWS. **O paper mostra que**:
- **Métodos avançados de engenharia** de software permitem o desenvolvimeento de interfaces de software **em conformidade com os frameworks semânticos**;
- Há uma **boa continuidade entre** os métodos de **design orientados a modelos e os conceitos de serviços web semànticos**;


### Opinião dos autores (INTERESSANTE):
- Criação de anotações semânticas deve ocorrer durante o desenvolvimento dos serviços.  
- O processo deve ser apoiado por metologias que combinem boas práticas de desenvolvimento dos serviços e de especificação semântica.

### Técnicas e notações utilizadas

- **Design em alto-nível da coreografia global**:
    - Modelos de processos envolvendo diversos atores/enterprises em **BPMN**;
- **Design do modelo de dados da aplicação cross-entreprise**:
    - uso de **diagramas entidade-relacionamento estendidos**, enriquecidos com **regras de derivação OQL**, para modelar a ontologia local da aplicação e para importar ontologias existentes. Exposição das ontologias para o **ambiente de execução WSMX**;
- **Design das interfaces do WS, da Plataforma de Integração e da Aplicação Front-End**:
    - Uso de **diagramas visuais** representando websites e serviços de acordo com **WebML**, incluindo primitivas de hipertexto específicas para invocação e publicação, bem ocmo representação explícita de conceitos de workflow.


### Geração automática
- **Coreografia global**, **objetivos**, **descrições dos WS** (em termos de capacidade de interface de coreografia) **e descrições dos mediadores** são **derivadas dos modelos BPMN e WebDL**.
- Implementação do **front-end** da aplicação e dos **serviços** é gerada automaticamente **a partir dos modelos de alto nível**.

------------------------------------------------------------

## Background

### Modelagem de processos de negócio
- Usa BPMN
    - alta adoção
    - intuitividade
    - efetividade na representação de processos de TI

### Model-driven Web Aplication design
- Usa WebDL
    - diversas visões de um site, cada uma dedicada a um dado grupo de usuários
    - visões podem ser substruturadas em áreas de páginas logicamente interrelacionadas;
    - Páginas popssuem unidades de conteúdo (peças atômicas de informaçaõ a serem representadas)
    - Páginas e unidades podem ser conectadas por links de diferentes tipos,que expressem todas as possíveis navegações lógicas entre eles.


### Serviços Web Semânticos
- Usa  framework WSMO:
    - modelo conceitual WSMO;
    - Linguagem WSML
    - ambiente de execução WSMX
    - ferramenta de modelagem WSMT


- **4 Principais elementos do WSMO*8:
    - **Ontologias** para prover semântica formal para a informação utilizada por outros componentes;
    - **Web Services** modelando aspectos funcionais e comportamentais, descritos em relação a três pontos de vista:
        - propriedades não-funcionais;
        - funcionalidades (capabilities) descritas por:
            - Preconditions
            - assumptions
            - ppostconditions
            - effects
        - Comportamento;
    - **Metas**: Objetivos que clientes podem ter ao consultar um WS;
    - **Mediadores**: para prover facilidades para interoperabilidade com outros elementos e prover suporte a interação orientada a metas eficiente com o WS.

- No trabalho, usam Glue, uim motor de descoberta compatível com WSMO.


-------------------------------------------------------------

## Runbning Example


------------------------------------------------------------------
## Design de aplicações de serviços web semânticos

### Modelo incremental/espiral para a impementação de uma aplicação Web semântica
![Modelo incremental/espiral para a impementação de uma aplicação Web semântica](./img/Brambilla2007-FasesDaImplementacaoDeUmWS.png)
Fases:
- **Especificação de requisitos**: ANALISTA da aplicação coleta e formaliza informação essencial sobre o DOMÍNIO DE APLICAÇÃO E FUNÇÕES ESPERADAS.
- **Design de processos**: esquematização em alto nível dos PROCESSOS INERENTES À APLICAÇÃO.
- **Design de dados**: ESPECIALISTA DE DADOS organiza das principais informações a respeito dos OBJETOS IDENTIFICADOS DURANTE A ESPECIFICAÇÃO DE REQUISITOS;
- **Design de Hipertexto**: TRANSFORMA os REQUISITOS FUNCIONAIS em um ou mais WS ou visões de WS, incorporando os métodos re manipulação e recuperação necessários
- **Descrição Semântica**: provê especificações semânticas à aplicação sendo desenvolvida. Fase nova e foco do trabalho, é requerida para compatibilidade com o WSMO.
- **Design da arquitetura**:
- **Implementação**;
- **Teste e avaliação**;
- **Manutenção e evolução**;

Na figura, **Design de processos**, **Design de dados**, **Design de Hipertexto** e **Descrição Semântica** são agrupadas em **Model Driven Design**.


### (...)

------------------------------------------------------------------

## Extração de descrições WSMO de Web Services


### Extração das capacidades do serviço
- BPMN e WebDL

### Extração da coreografia e oruestração
- Coreografia tipicamente precisa de uma anotação do designer para estabelecer todas as possíveis sequencias de interaçẽos com o servidor;
- Porḿe, ao menos uma coreografia pode ser extraída do BPMN pela análise da ordem de invoração das diferentes operações do serviço
- Não garante que todos os posssíveis cenários sejam considerados;


### Extração das  metas do usuário
- Combinação das informações do BPMN e do WebDL


### Design dos wwMediators com WebDL
- Uma lane é identificada como mediadora no BPMN
- informação básica do design do serviço de mediação é extraído
- Skeleton do mediador é automaticamente gerado
- Designer refina detales de  operações e parâmetros

-----------------------------------------------------------------

## Implementação
- desscreve as ferramentas usadas
    - WebRatio
    - BPMN Editor e WebML Generator
    - WSML description Generator
    - Glue Discovery Engineering

------------------------------------------------------------------

## Avaliação

### Avaliação de produtividade
![Porcentagem de código gerado](./img/Brambilla2007-PorcentagemDeCodigoGerado.png)


### Facilidade de uso
- Documentação automática
- checagem de modelos
- prototipagem rápida

### Escalabilidade
- 'garantida' pela aderẽncia a plataformas padrões estado-da-arte (J2EE, Jakarta Struts, WSMX)

------------------------------------------------------------------

## Trabalhos relacionados
(...)

------------------------------------------------------------------

## Conclusões
- Trabalho apresenta uma abordagem para o desenho de aplicaçoes de web semantica que utiliza abstrações existentes (usadas na engenharia de seerviços) para a extração semiautomática de compontentes WSMO.

- Trabalhos futuros:
    - incorporar os coceitos WSMO coo cidadões de primeira classe no processo de design, elevndo-os na hierarquia de design dos artefaos.
