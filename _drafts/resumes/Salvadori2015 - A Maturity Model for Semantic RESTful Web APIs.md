# A Maturity Model for Semantic RESTful Web APIs

Resumo de [Salvatori2015](doi:10.1109/ICWS.2015.98):
> Salvadori, Ivan, and Frank Siqueira. 2015. “A Maturity Model for Semantic RESTful Web APIs.” In 2015 IEEE International Conference on Web Services, 703–10. IEEE. doi:10.1109/ICWS.2015.98.


## Visão Geral
### Contribuição
- Um modelo de maturidade para API Web RESTful que avalia três dimensões ortogonais:
  - projeto (Design)
  - perfil (Profile)
  - Semântica (Semantics)
- Identifica estratégias para obter os mais altos níveis de maturidade propostos.

-----------------------------------------------------------
## Introdução
- REST é um estilo arquitetural para a integração de aplicações Web muito popular entre desenvolvedores;

- Um trabalho propõe que a maior parte das pesquisas é voltada para a evolução das tecnologias, enquanto **poucas pesquisas são criadas para a avaliação da tecnologia**.

- **Modelos de maturidade** provêem **referências úteis para a avaliação** de tecnologia.
  - Propõe critérios claros para a classificação de tecnologias existentes e suas implementações.


- **Um dos principais desafios à adoção de tecnologias semânticas** é a **falta de padrões e guidelines** para o desenvolvimento de aplicações com capacidades semânticas;
  - Um **modelo de maturidade** capaz de avaliar aplicações semânticas **poderia prover esses guidelines**;


### Contribuição
- Um modelo de maturidade para API Web RESTful que avalia três dimensões ortogonais:
  - projeto (Design)
  - perfil (Profile)
  - Semântica (Semantics)
- Identifica estratégias para obter os mais altos níveis de maturidade propostos.
-
-----------------------------------------------------------

## Background

### Semantic Web

**Extensão da Web** comum na qual os **dados disponibilizados estão anotados semanticamente**. Dessa maneira, a Semantic Web **permite que máquinas façam inferências sobre o significado dos dados publicados** na rede, facilitando a **integração e reuso** de dados.

**Dados disponíveis na Semantic Web** devem ser identificados por **identificadores uniformes de recursos** (URI) e devem ser **associados a descrições semânticas** por meio do uso de tecnologias padronizadas pela W3C, como o **Resource Description Framework** (RDF).


### REST

Estilo arquitetural cliente-servidor que define um conjunto de restrições de modo a aumentar a escalabilidade, disponibilidade e performance de aplicações Web.

**Recursos** REST são **abstrações que representam as informações manipuladas** por uma aplicação.
Recursos são **indentificáveis por URIs** e **acessíveis via uma interface padrão (verbos do protocolo HTTP)**.
**Recursos não são manipulados diretamente pela aplicação**, ao invés disso a **aplicação manipula representações desses recursos**.
Uma representação de um recurso consiste de uma imagem do estado interno desse recurso em um dado momento do tempo, representada usando um formato de compartilhamento de dados.

Serviços RESTful são *stateless*, ou seja, o servidor não deve consultar um histórico de interações com o cliente para prover uma dada resposta.
Neste sentido, cada recurso do servidor deve responder de maneira independente da informação trocada com o cliente no passado, embora o recurso possa ter um estado (informação interna) e mesmo um ciclo de vida próprio.


### Web API Design
