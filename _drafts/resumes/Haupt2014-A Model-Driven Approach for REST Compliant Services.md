# A Model-Driven Approach for REST Compliant Services

Resumo de [Haupt2014]:
> Haupt, Florian, Dimka Karastoyanova, Frank Leymann, and Benjamin Schroth. 2014. “A Model-Driven Approach for REST Compliant Services.” In 2014 IEEE International Conference on Web Services, 129–36. IEEE. doi:10.1109/ICWS.2014.30.


## Visão Geral

#### Background + gap:
- O estilo arquitetural REST provê vantagens;
- Seguir as restrições do REST é não trivial;
- Há diversas abordagens para a modelagem e descrição formal de aplicações REST;
- Porém, a maior parte dessas abordagens não dão atenção como a obediência ao REST pode ser reforçada por elas.

#### Proposta:
- **Abordagem orientada a modelos para a construção de serviços REST**;
- Modelo multi-camadas que permite o refoço (parcial) do compliance com estilo REST pela **separação de interesses** em modelos separados;
- Um **metamodelo multi-camadas para aplicações REST**;
- Um **modelo de papéis** para o desenvolvimento do serviço;
- Uma realização da metodologia como um editor Eclipse.


----------------------------------------------------------

## Introdução

#### REST
- **Estilo arquitetural** para sistemas hypermedia distribuídos;
- Tradicionalmente definido como um **conjunto de restrições**;
- Obediência às restrições REST **permite obter um conjunto de propriedades não-funcionais desejáveis**:
  - Escalabilidade;
  - Eficiência na rede;
  - Baixo acoplamento;
  - Evolutividade...

#### World Wide Web
- "Um grande sistema REST de hoje..."
- **Originalmente desenvolvida para que humanos** acessarem os documentos online, usando HTML para navegação;
  - Restrições REST eram razoavelmente bem atendidas;
- Com o tempo, **aumentou a necessidade de aplicações acessarem os recursos disponíveis**;
  - Restrições REST nem sempre são atendidas em sua totalidade;
- **Aplicações nem sempre aproveitam de todo o potencial da arquitetura** WWW + REST;
  - Pouco impacto a curto prazo;
  - Porém, a longo prazo deve ter impacto nas propriedaes não funcionais do sistema;

#### Orientação a modelos
- Técnicas propostas para **aperfeiçoar o desenvolvimento de aplicações complexas**;
- Abordagem geralmente leva a:
  - Melhor qualidade de código;
  - Menos erros;
  - Aumento do reuso de melhores práticas;
  - Código padronizado;
  - Maior manutenabilidade;
  - portabilidade por meio da separação dos modelos independentes de plataforma (PIM) dos modelos específicos de plataforma (PSM);


#### Proposta
- Abordagem orientada a modelos para o design e realização de aplicações REST;
  - Foco em como atingir o objetigo de seguir ao máximo as restrições do REST;
- Principais contribuições:
  - Conjunto de metamodelos para o design e a realização de aplicações REST;
  - Discussão de como esses metamodelos podem auxiliar na criação de aplicações que seguem os princípios REST;
  - Um modelo de papéis associado (role model);
  - Uma realização (protótipo) da abordagem esperada.


--------------------------------------------------------


## Restrições REST e sua Implementação na Arquitetura da WWW

O estilo arquitetural REST foi definido inicialmente para documentar as escolhas arquiteturais da WWW.

#### Restrições REST à arquitetura

- **Divisão de camadas entre cliente e servidor**: prescreve uma arquitetura em camadas com uma separação clara entre os componentes do cliente e do servidor.
  - Restrição válida por padrão, dada a arquitetura do HTTP.
- **Cacheabilidade**: dados de uma resposta do servidor podem ser marcados como cacheáveis ou não.
  - Componentes de cache podem ser incluídos em qualquer lugar entre cliente e servidor;
  - Restrição realizada pelo protocola HTTP;
- **Servidor Stateless**: prescreve que o desenvolvedor deve criar uma aplicação (hospadada em um servidor) de maneira que ela não dependa de estados.
  - Aplicações Stateless são conhecidas em outgros domínios, como JEE;
  - Importante quando o sistema é escalado baseado em paralelismo.
- **Interface Uniforme**: prescreve que todas as interações devem ocorrer baseadas em um conjunto fixo de métodos predefinidos.
  - Diretamente realizada pelos verbos HTTP;
  - Porém, é possível utilizar os verbos HTTP de forma errônea, ignorando sua semântica predefinida.
  - Assim, também é necessário que o desenvolvedor da aplicação entenda os métodos HTTP básicos e os use corretamente.



#### Restrições REST à interface

- **Identificação**: recursos devem ser identificáveis.
  - Implementada pelo HTTP, que mapeia URI a recursos.
- **Manipulação por meio de representações**: introduz a distinção entre recursos e suas representações.
  - Realizada pelo HTTP, no qual as mensagens podem ser tipadas e o tipo do corpo da mensagem (payload) é definido no header por um MIME media type;
  - Campos adicionais no header podem possibilitar a negociação do conteúdo, de modo que um mesmo recurso pode prover diferentes representações;
- **Mensagens auto-descritivas**: Uma mensagem deve conter toda a informação necessária para a compreensão de seu conteúdo.
  - Implementado no HTTP pela separação dos dados (incluídos no corpo da mensagem) e dos metadados (incluídos no header da mensagem);
  - Uso dos MIME types para indicar o tipo da representação de recurso contida no corpo da mensagem.
- **Hypertext As The Engine of Application State (HATEOAS)**: define que o estado da aplicação cliente é controlado diretamente pelos recursos que ela acessa.
  - A representação de um recurso deve conter todos os metadados que são necessários para que uma aplicação cliente possa interagir com o recurso, bem como se o recurso é (e como é) relacionado a outros recursos;
  - Uma aplicação HTML realiza essa restrição por meio de links e forms;
  - Porém, HTML é apenas um dos possíveis formatos de representação;
  - É responsabilidade do desenvolvedor da aplicação que  as representações sejam corretamente modeladas segundo esse princípio.
  - Geralmente, HATEOAS não é realizada corretamente, pois é um conceito novo aos programadores.

A WWW é a arquitetura de referência para o estilo arquitetural REST.

As restrições **Interface Uniforme** e **HATEOAS** são as que necessitam de maior atenção e **devem ser garantidas pela implementação de um serviço**. Porém, empiricamente é mostrado que a **garantir que o sistema cumpra essas restrições é algo não-trivial** e que **a maior parte dos sistemas que se dizem RESTful não o é realmente**.


---------------------------------------------------------

## Um metamodelo em camadas para aplicações REST

![Layered metamodel for REST][layeredMetamodelForREST]

- **Domain model**: provê a possibidade de modelar uma aplicação de forma independente do REST, **baseado no metamodelo mais adequado para representar um determinado domínio de aplicação** (Não define um metamodelo). É mapeado para um Atomic Resource Model ou para um Composite Resource Model. Esse mapeamento pode sofrer de *impedance mismatching*.
- **Composite resource model**: permite agrupar diversos Atomic Resource em uma Composite Resource de modo a reduzir a complexidade do resource model.
- **Atomic Resource Model**: é o modelo principal, permitindo o desenho de uma aplicação em termos de recursos, suas interrelações e as interfaces oferecidas por estes recursos.
  - Pode ser **transformado em uma descrição do WS** que servirá de interface para os clientes.
- **URI Model**: Define a estrutura de URIs para o Resource Model, i.e., qual URI será utilizada para identificar cada recurso.
  - Diretamente influenciada pela restrição HATEOAS que demanda o o uso de links (ou outros metadados incluidos na representação dos recursos) para conectar recursos. **(Um cliente do serviço não deve fazer qualquer assumção sobre a estrutura das URIs, mas apenas confiar nos links e metadados oferecidos como parte da representação do recurso.)**
- **Application Model**: consiste de um **modelo específico de plataforma** (PIM) derivado a partir do URI Model  e do Resource Model.
- **Código de aplicação**: passo final da abordagem orientada a modelos. Derivado do Application Model.



#### Modelo de papéis para o desenvolvimento

- **Especialista de Domínio**:
  - Responsável pela definição do **Modelo de Domínio**;
  - Tem de ser especialista no metamodelo escolhido para esse modelo;
  - Independente do REST e, pelo modelo de domínio ser opcional, também é um papel opcional.
- **Especialista REST**:
  - Responsável pelos modelos **Atomic Resource Model**, **Composite Resource Model** e **URI model**;
  - Deve ser familiarizado com orientação a recursos e possuir **profundo conhecimento** do princípio de **interface uniforme do REST**.  
  - Conhecimento básico do modelo de aplicação.
- **Especialista de Aplicação**:
  - Responsável pelo **Application Model** e pela **transformação em código**;
  - Deve ser familiarizado com as plataformas alvo.
  - Conhecimento básico do modelo de aplicação e da orientação a recursos.

[layeredMetamodelForREST]:./img/Haupt2014-layeredMetamodelForRest.png



## Discussão

...

## Realização

Constroem um plugin para Eclipse para suporte à metodologia.

- Tecnologias:
  - **Epsilon**
  - **Emfatic**: linguagem para definir modelos Ecore
  - **Epsilon Transformation Language (ETL)**: Transformações model2model;
  - **Graphical Modeling Framework (GMF)**: Diagramas gráficos
  - **Java Emmiter Templates (JET)**: Transformação Model 2 Code
  - **Jersey/JAX-RS**


## Trabalhos futuros

- Extensão aos metamodelos
  - ex: Resource Model atual atualmente foca em uma estrutura de recursos estática;
- Avaliação sistematica da abordagem
  - Comparação com outras abordagens verificando REST compliance;
