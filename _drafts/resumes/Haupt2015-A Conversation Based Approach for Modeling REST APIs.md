# A Conversation Based Approach for Modeling REST APIs

Resumo de [Haupt2015]:
> Haupt, Florian, Frank Leymann, and Cesare Pautasso. 2015. “A Conversation Based Approach for Modeling REST APIs.” In 2015 12th Working IEEE/IFIP Conference on Software Architecture, 165–74. IEEE. doi:10.1109/WICSA.2015.20.

## Visão Geral

#### Background
- O uso de uma API REST ocorre por meio da realização de **conversas** entre cliente e servidores;
- O estilo arquitetural REST propõe um conjunto de restrições que impactam em como uma conversa RESTful deve ocorrer;
- Mais informação sobre conversas RESTful em [Ivanchikj2016](./Ivanchikj2016 - Visual modeling of RESTful conversations with RESTalk.md)

#### Proposta
- Usar as conversas RESTful como ferramenta de modelagem de APIs REST em alto nível de abstração;
- Algumas conversações tem uma estrutura regular que pode ser abstraída em um ConversationType.
Some
conversations have a regular structure, which can be abstracted
into a conversation type. In this paper we have collected four
real-world conversation types, which can be realized with
different concrete HTTP request-response interactions.

#### Resultado
- Um metamodelo centralizado em interações que extende o trabalho de [Haupt2014](./Haupt2014-A Model-Driven Approach for REST Compliant Services.md);
- A caracterização de  conversas REST permite um novo ponto-de-vista na modelagem de APIs REST:
  - Permite aos usuários/desenvolvedores se focarem nas funcionalidades essenciais da API.
  - Quatro tipos de conversa são ccapturados pelo paper
- Transformações (expansões) dos tipos de conversa para sua descrição por extenso por meio de interações podem ser providas por meio de templates

-----------------------------------------------------------

## Introdução

-----------------------------------------------------------

## Conversas RESTful

- **Conversa:** Conjunto de atividades de comunicação entre dois ou mais participantes.

- Participantes de uma conversa RESTful:
  - Clientes
  - Recursos

- **primitivas de comunicação**: Interface uniforme (Métodos HTTP)
- Cada comunicação básica é **stateless**: O processamento da requisição pelo servidor não necessita de informação de requisições anteriores;
    - Todos os dados relevantes são contidos pela mensagem;
- **A conversa completa pode ser stateful**: o estado da conversa é **mantido pelo cliente**.
- Recursos podem **redirecionar** clientes a outros recursos. Duas formas de acessar um recurso são:
    - O cliente acessa um recurso inicialmente, começando uma conversa RESTful;
    - Cliente segue hiperlinks incluídos na representação de outro recurso, continuando uma conversa RESTful.

![RESTful Conversation](./img/Haupt2015-RESTfulConversation.png)


------------------------------------------------------------

## Tipos de Conversas RESTful

Seção coleta 4 exemplos de conversas RESTful comuns.

Conversas RESTful têm papel importante em interações cliente-servidor no mundo real e a seção apresenta exemplos que possam ser utilizados para verificar a abordagem apresentada.

### A. Redireção

1. Cliente manda requisiçao para R1;
2. Resposta de R1 contem metadados redirecionando cliente para R2;
3. Cliente lê resposta, identifica metadados;
4. Cliente envia requisição a R2.

![Redirect Conversation](./img/Haupt2015-redirect.png)


Examplos de respostas de redireção
![Examplos de respostas de redireção](./img/Haupt2015-redirectExample.png)


Um uso comum para redireções é o chamado **home document**, o **recurso raiz de uma aplicação REST** que **provê um conjunto de links para os principais recursos** da aplicação.


### B. Acesso a coleções de recursos

- **Coleções** são **recursos que armazenam outros recursos**;
- **Recursos** de uma coleção **podem ser listados ao consultar a coleção**
- Quando a representação de uma entrada é recuperada, um link para atualizar essa nova entrada é provido (edit link relation).
    - O link é seguido pelo cliente para updates do recurso.

![Conversa de gerenciamento de coleções](./img/Haupt2015-collectionManagementConversation.png)

Exemplo de gerenciamento de coleções
![Exemplo de gerenciamento  de coleção](./img/Haupt2015-exemploDeGerenciamentoDeColecao.png)


- Para coleções grandes, pode ser impraticável ao serviço retornar o índice inteiro da coleção em uma resposta apenas.
- Neste tipo de coleção, clientes podem ter de entrar em uma conversa com o serviço de modo a recuperar o íncice e localizar os recursos da coleção que estão interessados.
- A representação parcial de uma coleção grande irá conter  hyperlinks para o primeiro recurso, o último, o  anterior e o  próximo.
    - Dessa maneira, clientes podem determinar quando terão passado por toda a coleção e recuperar incrementalmente uma quatidade gerenciavel de entradas.

Exemplo de travessia de grandes coleções
![Exemplo de travessia  de coleção](./img/Haupt2015-exemploDeTravessiaDeGrandeColecao.png)

### C. Tentativa-Confirmação-Cancelamento

- **Try-Confirm-Cancel (TCC)**: padrão usado para criar serviços web RESTful que participam de transações atômicas distribuídas.
    - Clientes podem temporariamente modificar o estado de um recurso (ex: requisição de agendamento);
    - Transições de estado podem ser confirmadas posteriormente quando cliente conseguiu obter todos os recursos de interesse;
    - Assume que recursos são temporariamente reservados e vão reverter automaticamente para o estado inicial após um dado tempo sem confirmação;
    - Uma vez que o cliente inicie a confirmação, deve usar apenas requisições idenpotentes, de modo que qualquer número de tentativas possa ser utilizado para confirmar com todos os participantes da transação.
    - Partipantes que deram timeout respondem à confirmação com 404. Sucesso é respondido por 200.


![Exemplo de travessia  de coleção](./img/Haupt2015-tryConfirmCancel.png)



### D. REQUISIÇÕES DE PROCESSAMENTO LONGO
*Importantes para sistemas como o SemanticSCo*

- Em alguns cenários não é possível ao cliente esperar a requisição ser completada para receber uma resposta
    - Demora na computação
    - Timeout de rede


- Nestes cenários:
    1. Cliente envia a requisição original para um recurso **job manager**;
    2. Recurso aceita a requisição e imediatamente responde com um hyperlink que aponta para o **job resource** que o cliente irá usar para verificar o progresso de sua requisição;
    3. Periodicamente o cliente faz GET nesse job resource;
        - Se resposta 200 (ok), cliente repete a busca novamente depois de um tempo;
        - Se resposta 303 (redirect), o cliente é redirecionado para o recurso que contém a resposta obtida após processamento.
- É possivel ao cliente cancelar o processamento enviando DELETE para a job resource;
- Respostas podem também ser deletadas após serem completas.

![Exemplo do ciclo de vida de uma Long Running Request](./img/Haupt2015-exemploLongRunnigRequest.png)

![Sequencia de uma long running request](./img/Haupt2015-sewquenciaLongRunningRequest.png)




-----------------------------------------------------------

## Um metamodelo centrado em interações para serviços RESTful
**Utilizam a arquitetura de Haupt2014**

- Muitas APIs que se dizem RESTful não o são completamente;
- **Violar algumas restrições REST levam à perda de alguns dos atributos de qualidade das API REST-compliants**
    - cacheabilidade
    - escalabilidade
    - acoplamento fraco


- **Trabalho extende e refina Haupt2014**;
    - introduz um **metamodelo centrado em interações** para os **atomic resource models**;
    - introduz um **metamodelo centrado com conversação** para os **composite resource models**.

![Metamodelo centrado em interações (branco) com extensões para conversação (vermelho)](./img/Haupt2015-MetamodeloCentradoEmInteracoesComExtensaoConversacao.png)

A seção descreve as entidades do metamodelo e como utilizá-las para modelar um recurso ao nível de interação.
- **Resouce**: Conceito central. Pode ter um nome e ser marcada como ponto de entrada da API.
- **Métodos HTTP**: São todos modelados como entidades separadas no metamodelo (Exceto CONNECT e TRACE). São todas **subclasse-folha de InteractionBase** ou suas subclasses (quando suportam payload de requisição ou/e resposta).  
- **Relationship**: pode estar conectada à resosta das interações. São definidas relações de Criação e Navegação.
- **Grounding**: Pode prover informação adicional de como uma Relationship pode ser concretizada.

![](./img/Haupt2015-modelagemColecao-nivelInteracao.png)


- O propósito de abordagens MDD é obter código de aplicação.
- O metamodelo criado precisa ser tão complexo quanto apresentado para atingir esse objetivo e prover flexibilidade ao modelador;
- Porém, modelos podem se tornar complexos;
- Na seção seguinte, proverão contrutos de mais alto nível que elevará o nível de abstração, facilitando modelagem, melhorando inteligibilidade e escondendo detalhes repetitivos

-----------------------------------------------------------

## Modelando APIs REST baseadas em Conversação

- **Metamodelo centrado em conversação**
    - **Extende** o metamodelo centrado em interações
    - **Adiciona metaclasses** para Colections, TCC, LRR e Redireção;
    - Essas **metaclasses são utilizadas para simplificar o modelo de interação quando possível**, embora as metaclasses originais (estensas) estejam acessíveis.
    - **Modelos centrados em conversação são**, posteriomente, **transformados em modelos centrados em interação**.

![](./img/Haupt2015-sumarioDaAbordagemDeModelagem.png)


![](./img/Haupt2015-modelagemColecao-nivelConversação.png)


-----------------------------------------------------------

## Templates e suas expansões

- De modo a integrar o novo metamodelo na abordagem MDD definida em Haupt2014, **é necessário transformar o modelo centrado em conversação em um modelo centrado em interações**;
- Essa transformação é, geralemente, uma **Expansão**: um elemento de conversação é transformado em um conjunto de elementos que descrevem a essa conversaçao de forma extensa por  meio de interações.
- Essa expansão é guiada por **templates**, que **representam todas as escolhas de design** e pré-condições a serem levadas em conta durante essa expansão.
- Um **Template** só pode ser usado para um dado **ConversationType**
- Um **Template** pode prover um conjunto de **Expansion**, que por sua vez apresentam **Preconditions** e **Features**;
- **Precontitions** são avaliadas quando da escolha da expansão a ser executada;
- A **Expansion** é realizada por uma **Implementation**, o qual poderia ser qualquer artefato que implemente uma transformação de modelos;


![Metamodelos para templates](./img/Haupt2015-metamodelForTemplates.png)


![Exemplo de template de redireção com 3 expansões](./img/Haupt2015-templateExampleForRedirect.png)

![Exemplo de template para coleções com duas expansões](./img/Haupt2015-templateParaColecao.png)


- **Aplicação da expansão**:
    1. Para cada ConversationType contido no modelo da API REST, o template correspondente é selecionado (um template por ConversationType)
    2. Pré-condições para cada expansão são avaliadas e expansões inválidas são descartadas;
    3. Uma das expansões que sobram é escolhida
        - Pode ser manualmente ou usando outro critério, como as features da expansão;
    4. Finalmente, a implementação da expansão escolhida é recuperada e aplicada no modelo.


- A realização das expansões é feita por graph-transformation usando AGG.
