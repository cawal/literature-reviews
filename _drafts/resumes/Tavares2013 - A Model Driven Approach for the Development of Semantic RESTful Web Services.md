A Model Driven Approach for the Development of Semantic RESTful Web Services
================================================================================

Resumo de [Tavares2013]:
> Tavares, Nírondes A. C., and Samyr Vale. 2013. “A Model Driven Approach for the Development of Semantic RESTful Web Services.” In Proceedings of International Conference on Information Integration and Web-Based Applications & Services (IIWAS), 290–99. New York, New York, USA: ACM Press. doi:10.1145/2539150.2539193.

Visão Geral
-----------

**Background**:
- Serviços RESTful se tornaram importantes devido à sua simplicidade, heterogeneidade (?) e interoperabilidade;

**Problema**:
- Porém, o uso de diferentes linguagens e formatos de representação pode quebrar essa interoperabilidade;
  - Principalmente nas descrições semânticas desses serviços.

**Objetivo**:
- O paper propõe o uso de uma abordagem orientada a modelos (MDD) para o desenvolvimento de serviços web semânticos.

**Resultados**:
- Um metamodelo Ecore para a representação de serviços REST anotados semanticamente;
- Módulos ATL estranhos para a transformação de modelos desse metamodelo para WADL;
- Crença de que poderiam ser usados com outras linguagens de especificação semântica.

**Meta-informação:** Paper brasileiro, com vários erros gramaticais do inglês e **erros conceituais** em relação à MDA. Me pergunto se a citação desse trabalho seria depreciativa ao meu trabalho. **Schreider2011 não adiciona descrição semântica, mas apresenta um metamodelo REST aparentemente desenvolvido de forma mais cuidadosa.**



Introdução
----------

O Desenvolvimento de SWS em REST é uma atividade manual que requer muito esforço.

A complexidade dessa atividade aumenta dado a grande quantidade de tecnologias para a descrição sintática e semântica desses serviços.

A descrição sintática de um serviço REST é realizada em um documento web específico, o qual descreve o serviço em um formato compreensível por máquinas.

- Diversas linguagens estão disponíveis para a descrição sintática e disputam consenso:
  - **WADL** - Web Application Description Language;
  - **WRDL** - Web Resource Description Language;
  - **WDL** - Web Description Language;
  - **NSDL** - Norm's Source Description Language;
  - **WSDL** - Web Service Description Language;


- De forma semelhante, para a descrição semântica de serviços RESTful são disponíveis:
  - **SA-REST** - Semantic Annotation of Web
Resources;
  - **hRESTS with MicroWSMO** - HTML for RESTful Services com MicroWSMO;
  - **OWL-S**: Web Ontology Language for Services (with RESTfulGrounding);
  - **EXPRESS** - Expressing Semantic
RESTful Services;


- A variedade de tecnologias leva a problemas de interoperabilidade.
  - **Sistemas de mediação**:
  - **Tarefas de tradução**: tentam possibilitar interoperabilidade quando há heterogeneidade entre formatos de anotação semântica e linguagens de descrição sintática utilizadas;


- Abordagens orientadas a modelos vêm promovendo interoperabilidade:
  - Posicionam os modelos como elementos principais;
  - Modelos são a melhor forma de representar a estrutura e a semântica dos conceitos manipulados pelo sistema
  - Transformações de modelos como principais atividades de desenvolvimento.
  - Separação de Preocupações: Uso de metamodelagem e regras de transformação para promover:
    - reuso;
    - interoperabilidade;
    - adaptabilidade.


- **Proposta**:  Aplicação de MDD no contexto do desenvolvimento SWS RESTful.
  - Documentos da Sintaxe e semântica de um SWS RESTful podem ser desenvolvidos de forma independente e então mapeados para diferentes linguagens e formatos;
  - Ontologias e anotações semânticas podem ser definidas em um nível de abstração mais elevado pela definião de modelos e então serem utilizadas por diferentes instâncias do serviço.


Contexto Tecnológico
--------------------

- Web services
- Semantic web services
- RESTful Semantic Web Services
- MDD/MDA/MDE


Muito mal escrito. Erros conceituais em MDA. Provavelmente em outros lugares.


Metamodelo WSSR
---------------

Desenvolvido com base em :
Valverde, F., Pastor, O. 2009. Dealing with REST Services
in Model-driven Web Engineering Methods. V Jornadas
Científico-Técnicas en Servicios Web y SOA, JSWEB.

Adiciona representação de informação semântica provinda de uma ontologia.

Não é relacionado a uma linguagem de implementação/descrição.

![Metamodelo. A única parte que parece poder trazer algum conhecimento neste trabalho.](./img/Tavares2013 - metamodelo WSSM.png)


RESTO DO PAPER
--------------

Um exemplo, umas transformações ATL estranhas (usam helpers e não rules) e outras coisas que não sei se valem a pena perder tempo.
