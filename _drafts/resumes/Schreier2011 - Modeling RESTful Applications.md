Modeling RESTful Applications
=============================

Resumo de [Schreier2011] (Conference paper):
> Schreier, Silvia. 2011. “Modeling RESTful Applications.” In Proceedings of the Second International Workshop on RESTful Design (WS-REST), 15. New York, New York, USA: ACM Press. doi:10.1145/1967428.1967434.

Visão Geral
-----------

- REpresentational State Transfer (REST) cresce em importância;
- Frameworks para implementação de serviços RESTful são abundantes;
- Porém, há falta de suporte às fases iniciais do processo, como análise e design;
- É necessário um metamodelo apropriado para o desenvolvimento de aplicações RESTful;
- **O trabalho apresenta um metamodelo para a definição de uma aplicação RESTful**;
- **Apresenta um exemplo de modelagem** de uma aplicação RESTful utilizando este metamodelo. **Não implementa esse exemplo**.
- **Metamodelo não lida ainda com** aspectos avançados como:
  - autenticação;
  - StatusCodes e Metadata;
  - Condições de transição entre estados.








Introdução
---------

**REST**: Introduzido em 2000 por Roy Fielding como estilo arquitetural para sistemas de hypermídia distribuídos.
- Abstração da arquitetura básica do HTTP;
- Concentra-se nos conceitos, não nos detalhes técnicos e sintaxe.  
- Permite alcançar:
  - Maior performance e escalabilidade;
  - Menor acoplamento -> maior interoperabilidade;
  - Maior reuso.  
- Fielding define REST como tendo um alto nível de abstração.

Muitos frameworks provêem suporte à implementação de aplicações RESTful. Porém, há uma falta de suporte e de melhores práticas para outras partes do desenvolvimento, como análise e modelagem da aplicação.

O metamodelo apresentado busca criar uma terminologia para a modelagem e implementação de aplicações RESTful.
-  Essa terminologia pode auxiliar no entendimento e no desenvolvimento de aplicações RESTful.






Trabalhos relacionados
----------------------

- **REST**:
  - Fundações:
    - Fielding
  - Exemplos de melhores práticas
    - Tikov
    - Richardson & Ruby
  - hRESTS como solução para a falta de API machine-readable de serviços RESTful. Forma fácil de enriquecimento de documentações human-readable
    - Kopecký
  - **Abordagem para a reengenharia de sistemas legados como RESTful WS**: Liu


- **MDD**:
  - Processo para design de RESTful services que tenham foco na identificação de recurso baseada em modelos e para a geração de descrições WADL: Laitkorpi
  - A maior parte das publicações utiliza seus próprios **metamodelos informais**, ajustado para as próprias necessidades;


- **Fraquezas do REST**:
  - Pautasso compara REST com serviços WS-\* baseado em decisões arquiteturais e inclui pontos fortes e fracos das duas variantes. Indicam como fraquezas:
    - confusão sobre melhores práticas;
    - falta de geração de código stub para clientes;
    - esforço aumentado para manter múltiplos formatos de epresentação;
    - falta de métodos padronizados para design e documentação;
  - Vinoski elenca como fraquezas:
    - falta de suporte ferramental
    - falta de entendimento
    - falta de melhores práticas
    - maior parte dos desenvolvedores estão acostumados a linguages de programação que utilizam interfaces específicas (?)




Básico sobre metamodelagem
--------------------------

- **Metamodelo**: especifica a estrutura e significado de um modelo. Define:
    - **Sintaxe abstrata**: os elementos possíveis em um modelo e as relações entre esses elementos;
    - **Semântica estática**: restrições para modelos bem formados;
- Uma **sintaxe concreta** pode ser provida por uma linguagem textual ou visual (gráfica).
- Embora um metamodelo possa ser definido utilizando uma linguagem natural, é necessária uma definição formal por meio de um **meta-metamodelo** para prover suporte ferramental adequado. Um meta-metamodelo geralmente é descrito por si mesmo (descrição recursiva).


- **Meta-metamodelos** muito utilizados são o **OMG's MOF** e o **Eclipse's Ecore**.
- **O trabalho escolhe o Ecore** pela **simplicidade** e pela existência de suporte ferramental.
    - **MOF só deveria ser usado quando UML é escolhida coo metamodelo** por só haver suporte de ferramentas à esta, e não ao MOF.
    - Porém, **UML nao seria apropriada para modelar aplicações RESTful** porque ela contém **conceitos orientados a objeto que entram em conflito com as restrições REST**, tais como interface uniforme.


- Conceitos do Ecore de interesse são:
    - **EClass**: Representa um conceito e é identificado por um nome. Pode definir atributos e referências, bem como herança;
    - **EDataType**: Representa tipos primitivos e é identificado por um nome.
    - **EAttribute**: É identificado por um nome e tipado por um EDataType. Define cardinalidade desse atributo.
    - **EReference**: Representa referências a outros conceitos. Pode ser modelado como uma referência unidirecional ou bidirecional (devendo ter uma referência inversa no alvo). Também pode modelar relações de Containment, definir cardinalidade e ordenação.




Metamodelo REST
---------------

Dividido em modelagem estrutural e comportamental.

Modelagem estrutural: Descreve tipos de recursos, seu atributos e relações, bem como suas interfaces e representações.

Modelagem comportamental: Possibilita descrever o comportamento utilizando máquinas de estado.

O metamodelo não permite ainda a modelagem dos detalhes da representação [dos recursos].




#### Modelagem estrutural ###

Primeiramente foram identificados os elementos estruturais do metamodelo a partir do trabalho do Fielding.

Fielding nomeia **resource** como um elemento principal.

Trabalho usa **resource** para falar de *elementos concretos** e **resource types** para descrever aspectos em comum de múltiplos resources.

**Descrição dos Resource Types:**
- **primary resource**:  core concepts of the modeled domain, e. g., pictures and albums;
- **subresource**: part of another resource, which should also be addressable directly;
- **list resource**: list of all concrete resources of a primary resource;
- **filter resource**: list of concrete resources with desired properties;
- **projection resource**: contains only a subset of attributes of another resource;
- **aggregation resource**: aggregates attributes of different resources to reduce the interaction amount;
- **paging resource**: splits large resources in different pages;
- **activity resource**: stands for one single step of a workflow.

![Imagem com a hierarquia de tipos de recusos][resources]


Por causa da interface uniforme, atividades que vão além do CRUD precisam ser modeladas de forma diferenciada. Para isso, pode-se usar **ActivityResourceType**. ActivityResourceType é, normalmente, uma  nominalização de uma atividade.

![Imagem com as principais metaclasses do metamodelo estrutural][structural metamodel]



#### Modelagem comportamental ###


Utiliza máquinas de estados finitos determinísticas para descrever o comportamento dos ResourceTypes.

Permite representar o estado atual de um recurso e definir como um recurso reage a uma dada requisição.

O comportamento concreto de um método é representado de forma imperativa.


![Imagem com as principais metaclasses do metamodelo comportamental][behavioral metamodel]



- Todo `Resource` define um conjunto de `States`;
- Um `State` pode ser fonte de N `Transitions` de saída;
- Uma `Transition` define um ou mais `Triggers`;
- Uma `Transition` pode definir uma `Condition`, mas `Conditions` não estão modeladas ainda no metamodelo REST;
- Toda `Transition` pode definir um `BehaviorSpecification` para adicionar um comportamento à transição.
- De forma semelhante, todo `State` pode definir 2 `BehaviorSpecifications`: um ao entrar no estado e outro ao sair do estado;
- Todo `State` define um conjunto de `Methods` (cada qual com seu `MethodType` HTTP) suportados durante aquele estado;
- Cada `Method` define seu comportamento por uma única `Action`;
- Diferente tipos de `Action` são providas, incluindo uma `ActionSequence`;
- Mais explicações no artigo;






Exemplo de uso
--------------

1. Modelagem estrutural:
  1. Identificação dos PrimaryResourceTypes;
  2. Adição de SecundaryResourceTypes para o manuseio de dados secundário
  3. Uso dos demais resource types conforme necessário;
  4. Escolha dos MediaTypes e Representations;
2. Modelagem comportamental:
  1. Definição do comportamento dos ResourceTypes;

![Exemplo de modelagem estutural][example resource types]

![Exemplo de modelagem comportamental][example states]


[resources]:./img/Schreier2011-hierarchy_of_resources.png
[structural metamodel]:./img/Schreier2011-core_structural_metamodel.png
[behavioral metamodel]:./img/Schreier2011-core_behavioural_metamodel.png
[example resource types]:./img/Schreier2011-exampleResourceTypes.png
[example states]:./img/Schreier2011-exampleResourceTypes.png
