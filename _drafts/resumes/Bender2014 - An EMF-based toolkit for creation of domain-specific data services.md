# An EMF-based toolkit for creation of domain-specific data services

Resumo de [Bender2014]:
> BENDER, A.; BOZIC, S.; KONDOV, I. An EMF-based toolkit for creation of domain-specific data services. Model-Driven Engineering and Software Development (MODELSWARD), 2014 2nd International Conference on. Anais... [S.l: s.n.]. , jan 2014


![Techonology stack](./img/Bender2014-TechnologyStack.png)



## Visão Geral
### Problema
- Desenvolvimento de aplicações para workflows compostos para ciência e engenharia é algo **custoso**:
    - grande **heterogeneidade** de representação de dados e de interfaces de acesso;

### Proposta
- **Toolkit genérico** que permite ao **especialista de domínio**:
    - **desenvolver modelos** de dados;
    - **gerar serviços** (autocontidos) **de acesso aos dados**.
- Metamodelo **Ecore** que pode ser utilizado para criar modelos de dados específicos de domínio
- **Serviços de acessos aos dados** gerados permitem o acesso à instâncias dos dados modelados residindo em recursos distribuídos;
    - uso de **interfaces Web**.


-----------------------------------------------------------
## Introdução


### Background
- **Aplicações para simulação e análise de dados** em ciência e engenharia:
    - Geralmente **construídas como workflows genéricos**
    - Combinam **múltiplos componentes** de software;
    - Componentes **originados de diversos domínios** diferentes;


- A **complexidade para o desenvolvimento** dessas aplicações **cresceu grandemente** através dos anos.
- **Múltiplos programas** disponíveis para uma **mesma função**;
- Tempo necessário para implementar mudanças aumenta bastante;


- **Desenvolvimento necessita de**:
    - **Coordenação de especialistas** dos diversos domínios; ou
    - **Aprendizado de múltiplos códigos** específicos de domínio por parte de um pesquisados.


- **Ambientes de análise atuais** ainda são de uso e **manutenção difícil a não-especialistas em engenharia de software**:
    - **Porém análises e simulações geralmente são delineadas e executadas por cientistas**;
    - **Portanto, uma estrutura fácil** de ser utilizada e mantida por estes cientistas **precisa ser provida**.


### Proposta

- Uso de **Service Oriented Architecture e Model Driven Engineering**
    - **Desenvolver** sistemas **em um alto nível** de abstração técnica;
    - **Permitir** ao especialista de domínio **integrar componentes existentes sem se preocupar com tecnicalidades**;


### Desafios à construção de sistemas de gerenciamento de workflows

#### Dataflow:

- **Troca de dados entre passos** do workflow:
- Modelagem de dados é extremamente importante para evitar problemas no dataflow;
- Dados complexos deveriam ser armazenados de forma que todos os passos pudessem acessá-los facilmente;


- Porém, **na prática muitos programas não podem utilizar as mesmas fontes de dados**:
    - Representações de dados mutualmente incompatíveis;
    - Formatos de dados heterogêneos;
    - Interfaces de acesso não uniformes


- **Estratégia comum para integração do dataflow: CONVERSORES**
    - Uso de conversores entre os passos do workflow
    - Convertem os dados para o uso da próxima aplicação.


- **Estratégia insatisfatória** para o caso geral:
    - Operam em um **conjunto limitado de formatos** suportados
    - Requer a **reimplementação (tediosa) de conversores para todas as combinações** de passos do workflow;
    - Conversões múltiplas em **dados grandes pode se tornar um gargalo do sistema**.
    - **Conversão é restrita aos dados no sistema de arquivos**, precisando de manutenção de metadados junto aos dados da aplicação.



- **Estratégia mais eficientes (porém não trasnferíveis) foram criadas** para tratar o dataflow em diversos domínios de aplicação;
    - Algumas utilizam abordagens orientadas a modelos;
    - **Exemplos no paper**.

### Gap
- **Um modelo de dados genérico (metamodelo) e uma ferramenta genérica são necessários**
    - permitir qualquer especialista / workflow designer a desenvolver modelos de dados específicos do domínio de aplicação;
- **Dados devem ser feitos acessíveis a cada aplicação individual via uma interface independente de linguagem**
    - Ex: um web service;
    - Permitir a implementação rápida de pré- e pós-processadores e para a construção de workflows por meio de componentes padrões;


### Resultados
- **Solução genérica para modelagem e manutenção de dados para workflows científicos em uma forma simples e uniforme**;
    - Desenvolvimento de um **conceito anterior (Bozic and Kon-dov, 2012)** criando uma nova solução baseada em SOA e MDE usando padrões web mais recentes
    - **Baseado em EMF, um metamodelo customizado e modelos de domínio**;
- **Descreve a  implementação** do framework;
- **Discute** funcionalidade e aplicabilidade geral;


-----------------------------------------------------------

## Análises de Requisitos

Consideram experiências anteriores:

**Requisitos**:
1. Ser genérico e **independente de domínio**
2. Agir como uma **ponte para o acesso de armazenamento distribuído e heterogêneo**
    - Prover duas interfaces: uma do lado da aplicação e outra do lado do armazenamento
3. Prover um **ambiente de modelagem** (gráfico)
4. Prover **geração automática de código completa** de todos os componentes;
5. prover uma **interface de acesso independente de linguagem** (como WS)
6. Prover uma **interface abstrata de acesso ao armazenamento**,  permitindo às aplicações conectarem a recursos de armazenamento de dados heterogêneos e distribuídos.



-----------------------------------------------------------
## Arquitetura do framework

### Metamodelo do domínio

- Um **metamodelo personalizado** foi criado para a camada M2
    - Restringido ao **mínimo necessário para modelar e gerenciar dados**;
    - Propósito: **isolar o especialista de domínio de complexidades** desnecessárias da UML


- **Objetos da realidade (M0)** representam **unidades de dados** específicos de domínio **que os usuários queiram tornar persistentes** em instâncias de armazenamento.
    - **Significado desses dados depende do domínio correspondente**.

- **Principais elementos**:
    - **DataEntity**: entidades de dados concretos. Possuem referências e atributos;
    - **Attributes**: Possuídos por DataEntities, são tipados por **EDataType**;
    - **References**: Possuídos por DataEntities,, referenciam outras DataEntities (modelam relacionamentos e dependências);
    - **Package**: Agrupam DataEntities, definem informação de indentificação (como URI);

- Planeja-se estender o metamodelo para herança e verificar em domínios mais extensos;

![Custom metamodel for the M2 metamodeling layer](./img/Bender2014-CustomMetamodel.png)





### Geração dos serviços

1. Especialista usa um editor gráfico para criar um modelo de domínio
    - Também define um conjunto de propriedades necessárias para ganhar acesso ao recurso de armazenamento alvo (URL de conexão e credenciais, p.e.)
2. Também é definido um conjunto mandatório de propriedades usadas para a interface do web service;
3. O metamodelo, o modelo de domínio e as propriedades do serviço são utilizados por um gerador automático de serviço.

![From domain model to data access service](./img/Bender2014-FromDomainModel2DataAccessService.png)




### Serviços de acesso aos dados

- SOA
- **Principal componente: núcleo (kernel) em 3 camadas**:
    - **Camada de persistência**: Mapeia objetos de dados a entidades do armazenamento;
        - Tipos de armazenamentos disponíveis: relacional, baseado em grafos ou árvores, baseado em web ou baseado em documentos;
    - **Camada de representação**: serializa/deserializa objetos de dados a partir dos formatos de transporte (como XML ou JSON).
    - **Camada de recurso**: define a interface do recurso Web (controler)
        - Camada mais complexa (?)
        - Controller que mapeia as operações do serviço às operações CRUD da camada de persistência.
        - Também responsável para entregar e receber objetos de dados da camada de representação.  

![3 layers of the data access service kernel](./img/Bender2014-DataAccessServicesLayers.png)



-----------------------------------------------------------
## Seleção da tecnologia

![Techonology stack](./img/Bender2014-TechnologyStack.png)

Alguns detalhes:

- **Acceleo**: Transformação M2T, implementação do padrão MOFM2T;
- **JDO (Java Data Objects)**: Framework orientado a anotações para mapear objetos java a entidades de armazenamento.
    - Suporte a vários tipos de sistemas armazenamento (relacional, web, documentos,mapeamentos...)
    - Implementação de referência: DataNucleus
- **JAXB**: Usado para fazer binding de objetos java e XML, e vice-versa;
- **JAX-RS**: Especificação para o desenvolvimento de serviços REST em JAva
    - Implmentação de referência: Jersey
- **Jetty**: poderia ser Apache Tomcat


-----------------------------------------------------------

## Implementação do toolkit

- Como plugins Eclipse
- Providos por um update site
- Wizards e interface gráfica são Providos
- Durante a geração, o usuário pode escolher entre gerar apenas o serviço ou criar um servidor Jetty já com o serviço instalado
    - Distribuem o Jetty junto

![5 step build process for a data access service](./img/Bender2014-buildProcessForADataAccessService.png)

-----------------------------------------------------------

## Benefícios dos Data Access services

### Deployment
- Facilitado:
    - Cria um arquivo WAR padrão, usável em servidores Tomcat, Jetty, Glassfish
    - Pode criar um servidor Jetty completo

### Segurança
- **Servidores de aplicação provêem vaŕias funcionalidades para a segurança, como encriptação, autenticação e autorização**
- Encriptação via **SSL**
- **Autenticação e autorização REST**:
    - Basic authentication
    - Disgest authentication
    - Outros protocolos de autenticação podem ser usados (Shibboleth, chave pública, kerberos...)

### Design evolucionário
- Modelos de domínio mudam constantemente
- Componentes precisam evoluir junto
    - **Mudanças no cliente podem ser minimizadas pela geração de código cliente usando a descrição do serviço web** que é automaticamente publicada pelo Data Access Service;
    - **JDO** possibilita o suporte ao design sem esquema de bancos de dados e **é capaz de lidar com mudanças no esquema em bancos de dados** que estes esquemas existam;


### Acesso ao serviço
- **REST**
- Descrito por **WADL**
- Formato de transporte: XML (JAXB)
- **XSD Schema** é provido pelo serviço para descrever a representação de transporte
- **Jersey pode ser usado para criar stubs do serviço para clientes** baseado na descrição WADL e XSD;
- Aparentemente, **dados são convertidos para o modelo de domínio**, que é **usado como formato padrão para o transporte** e, então, são **transformados de volta para o formato desejado/necessário no receptor** (lifting/lowering)


### Performance

![Throughput dos Data Access Services em diferentes backends de armazenamento](./img/Bender2014-throughputDAS.png)


-----------------------------------------------------------

## Trabalhos relacionados

- Não conhecem nenhum ambiente que satisfaça todos os requisitos elencados na seção 2;
- Os benefícios práticos de MDE e SOA em domínios científicos e de engenharia ainda são limitados;
- Soluções existentes ou são muito específicas (e não transferíveis entre cenários/domínios) ou são muito genéricas (e não acessíveis aos usuários)

- **SIMPL framework architecture: extensão aos sistemas de gerenciamento de workflows científicos existentes para prover abstração do gerenciamento de dados;**   
- MEMOPS (MEtaMOdel Programming System): geração de código
- CONNJUR integrated environment:
- Universal Access Layer: Armazenamento universal para o Kepler workflow system;
- MORSA: Acesso escalável de modelos grandes pelo carregamento sob-demanda em uma persistência NoSQL;
