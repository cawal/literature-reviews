# Visual modeling of RESTful conversations with RESTalk

Baseado em [Ivanchikj2016]:
> Ivanchikj, Ana, Cesare Pautasso, and Silvia Schreier. 2016. “Visual Modeling of RESTful Conversations with RESTalk.” Software & Systems Modeling, May. Springer Berlin Heidelberg, 1–21. doi:10.1007/s10270-016-0532-2.

## Visão Geral
#### Background:
- APIs RESTful são decompostas em recursos múltiplos;
- **Podem ser necessárias diversas interações cliente-servidor** até deixar um recurso REST em um dado estado desejado.
  - => **RESTful Conversation**;
- RESTful Conversations são **guiadas por controles de hypermedia** (como links)

#### Problema:
- => Clientes podem **não ter conhecimento prévio de todas as interações necessárias** a um objetivo;
- <= **Documentação** existente para APIs RESTful descreve a estrutura estática da interface, expondo detalhes de baixo nível (HTTP) mas dando **pouca atenção a modelagem conceitual de alto nível da dinâmica da conversa RESTful**;

#### Gap:
- **Detalhes de baixo nível podem ser abstraídos**...
  - durante a fase de design da API;
  - durante a tomada de decisão de qual API usar.

#### Proposta:
- **Modelos visuais** podem ser utilizados para aumentar o **entendimento** e a **eficiência** do desenvolvedor;

#### Resultados
- Propõe **RESTtalk**:
  - **extensão** ao diagrama de **Composição BPMN**
  - Torna os diagramas BPMN mais expressivos e expressivos para o domínio REST;
- **Reportam um survey** que fizeram sobre a maturidade do domínio sobre DSLs.

----------------------------------------------------------
## Introdução
- Web APIs permitem que sistemas interajam uns com os outros em uma rede => acesso a Web services.
- Houve uma mudança recente na engenharia de software em direção ao desenvolvimento de APIs:

###### Paradigma API-First:
- Promove o desenvolvimento ágil de APIs e de serviços Web;
- Necessidade de feedback rápido sobre o design da API;

###### Importância da visualização
- **Fases iniciais de engenharia** podem ser **facilitadas por meio da visualização do comportamento da API**.
- A **documentação** da API pode **melhorar a usabilidade e o aprendizado** da API
  - **Aprendizado** de APIs é uma das partes **integrantes do trabalho de um desenvolvedor**;
  - Pesquisa indica que **78% dos desenvolvedores aprendem** uma API **pela leitura da documentação**;
  - Respondentes reportam que **falta nessas documentações uma apresentação em alto nível** de abstração **sobre o comportamento da API** de modo a ajudá-los a usar a API para alcançar seus objetivos.


###### RESTful Conversations

- **Conceito importante ao raciocinar sobre como um cliente usa uma API REST**;
  - Tamanho e complexidade da API cresceria fora de controle se operações fossem adicionadas de modo a satisfazer as necessidades de todos os clientes;
  - => **Apenas um conjunto mínimo de operações costuma ser provido**, de modo a satisfazer todos os clientes;
  - => **Clientes compõe as operações** desse conjunto em RESTful Conversations, de modo a alcançar seus objetivos;


- **Ferramentas atuais para a documentação** de APIs REST têm foco nos aspectos estruturais e de dados da API apenas => **não capturam a dinâmica de interação** entre cliente e servidor;
  - **RAML**
  - **Swagger**
  - **Blueprints**
  - **Mashape**


- **Necessidade de uma notação específica de domínio** para representar uma conversa RESTful (RESTtalk);
  - Testaram anteriormente o uso de diagramas de sequência UML;
  - Precisavam de algo que permitisse **visualizar todas as possíveis intgerações** que pudessem ocorrer em uma RESTful conversation;
  - **Ponto de partida: Diagramas de coreografia BPMN**
    - Já utilizado para modelar interações entre múltiplos grupos;
    - Padrão ISO desde 2013;


- **Obtiveram feedback positivo sobre o RESTtalk**;
  - 35 participantes
  - quase todos desejam usar em seus projetos
  - considerado igual ou mais compreensível e concisa que as notações textuais ou gráficas que usam.



----------------------------------------------------------

## Restful Conversations

REST é um estilo arquitetural cliente-servidor com uma série de restrições que afetam a forma com que cliente e servidor interagem.

###### Statelessness:
- Requer que a requisição do cliente carregue TODA a informação necessária para que o servidor entenda tal requisição.
  - Servidor não precisa lembrar o estado da conversa;
  - Como consequência, toda interação em uma arquitetura REST é iniciada pelo cliente.


- Clientes são interessados em maipular recursos hospedados pelo servidor;
  - recurso: abstração conceitual de alguma informação ou serviço que possa ser nomeado e identificado como relevante ao cliente;


- Cliente descobre o URI do recurso dinamicamente a partir da resposta do servidor, que pode redirecionar o cliente a recursos relacionados;
  - A resposta do servidor pode conter zero ou mais links de acordo com o estado do recurso requisitado;
  - Esses links podem ser parametrizados de maneira que o cliente possa construir dinamicamente a URI para a próxima requisição;
  - Ou o cliente pode simplesmente seguir links, como é previsto pelo HATEOAS.


###### RESTFul Conversations
- Dadas as retrições da arquitetura REST, RESTful Conversations podem ser vistas como um tipo específico de conversas baseadas em mensagens com as seguintes características:
  - Interações são sempre começadas pelo cliente;
  - Requisições dos clientes são endereçadas a recursos;
  - Quando um serviço está disponível para processar requsições dos clientes, toda requisição de um cliente é seguida de uma resposta;
  - Respostas podem conter links de hipermídia, que podem ser seguidos pelos clientes;
  - Toda requisição é autocontida e independente das anteriores;
  - Interface Uniforme de um conjunto limitado de métodos que o recurso pode prover suporte

###### Divisão de responsabilidade do REST Conversation

- Cliente sempre começa conversa
- Servidor guia a conversa por meio de hyperlinks
- Cliente geralmente pode acumular as URI recebidas durante toda a conversa ou em conversas anteriores
- Cliente pode decidir entre reenviar uma requisição após um timeout ou desistir.


-----------------------------------------------------------

## Modelagem visual de  RESTful Conversations

- Inicialmente, foi considerado o uso de coreografia BPMN para a modelagem das conversas REST.
  - Têm foco na troca de mensagem de modo a coordenar  a atividade de dois ou mais participantes.
  - Descrevem precisamente a ordem parcial em que as interações devem ocorrer;


  ![Exemplo usando coreografias BPMN][fig1]

- Porém Lindland et al. propõem que a qualidade de uma linguagem, em modelagem conceitual, tem como aspecto importante ser apropriada ao domínio de aplicação.
    - **"Linguagem deve ser poderosa o suficiente para exprimir qualquer coisa do domínio, porém não mais que isso"**.


- Nesse sentido, propuseram um conjunto de pequenas modificações para a linguagem, tornando-a mais apropriada para a modelagem de conversar REST.

![Exemplo de uma conversa RESTful usando RESTtalk][fig2]


#### Modificação 1: Reposição da notação de atividade BPMN

- No REST, a interação é sempre iniciada pelo cliente E toda requisição é seguida de uma resposta.
- O conteúdo da mensagem é de interesse particular, pois define:
  - o recurso;
  - a ação a ser tomada pelo servidor;
  - futuros rumos da conversa.
- => A atividade BPMN é substituída de modo a representar de melhor forma essa interação

![Modificação 1 do BPMN: Reposição da notação de atividade BPMN][fig3]

#### Modificação 2 do BPMN: Permitindo diferentes respostas do servidor

Request-response bands podem ser separas quando há uma divergência de caminhos dada a possíveis respostas do servidor.

![Modificação 2 do BPMN: Permitindo diferentes respostas do servidor][fig4]

#### Modificação 3 do BPMN: Hyperlink flow

Indica como hyperlinks são descobertos por cliente na resposta recebida e como o cliente pode usar eles para navegar entre recursos relacionados.

Dada a restrição de HATEOAS do REST, clientes não devem ser forçados a adivinhar URIs ou obter essas URIs de conhecimento que não venha da resposta.

![Modificação 3 do BPMN: Hyperlink flow][fig5]

#### Construtos centrais do RESTtalk

1. Evento de iniciação
2. Atividade contendo mensagens de requisição e resposta
3. hyperlink flow
4. Gateways de controle de fluxo AND, OR e XOR para a divisão de caminhos após uma decisão do cliente;
5. gateways de controle de fluxo XOR, OR e AND para a conversão de caminhos após uma divisão;
6. Divisão exclusiva dado diferentes respostas do servidor;
7. timeout da resposta, depois do qual o cliente pode tomar a decisão de tentar novamente ou desistir.
8. Evento final, marcando o fim, da conversa após algum objetivo ser alcançado.

---------------------------------------------------------
## Guidelines

Simplificações e guias de estilo são apresentadas na seção 3.2.

![Diagrama RESTtalk simplificado usando as regras da seção 3.2.1][fig6]


-----------------------------------------------------------
## Fontes de interesse

###### Qualidade de ferramentas e linguagens de modelagem

Green,T.R.G., Petre, M.:Usability analysis of visual programming environments: a cognitive dimensions framework. J. Vis. Lang. Comput. 7(2), 131–174 (1996)


Lindland, O., Sindre, G., Solvberg, A.: Understanding quality in conceptual modeling. IEEE Softw. 11(2), 42–49 (1994)

Moody,D.: The “Physics” of notations: toward a scientific basis for constructing visual notations in software engineering. IEEE Trans. Softw. Eng. 35(6), 756–779 (2009)

###### Modelagem de API REST


Haupt, F., Leymann, F., Pautasso, C.: A conversation based approach for modeling REST APIs. In: Proceedings of the 12th WICSA 2015. Montreal, Canada (2015)


Li, L., Chou, W.: Designing Large Scale REST APIs Based on
REST Chart. In: 2015 IEEE International Conference onWeb Ser-vices (ICWS), pp. 631–638 (2015)


Mitra, R.:Rapido: a sketching tool forWeb API designers. In: Pro-ceedings of the 24th InternationalConference onWorldWideWeb. WWW’15 Companion, pp. 1509–1514. Florence, Italy (2015)


Nikaj, A.,Mandal, S.,Pautasso,C.,Weske, M.:From choreography diagrams to RESTful interactions. In: The Eleventh International Workshop on Engineering Service-Oriented Applications, pp. 3–14 (2015)


Nikaj, A., Weske, M.: Formal specification of RESTful choreog-raphy properties. In: Proceedings of the International Conference on Web Engineering (ICWE’16), pp. 365–372. Springer, Berlin (2016)


Pautasso, C., Ivanchikj, A., Schreier, S.: Modeling RESTful conversations with extended BPMN choreography diagrams. In: Weyns, D., Mirandola, R., Crnkovic, I. (Eds.) European Confer-ence on Software Architecture, LNCS, pp. 87–94. Springer, Berlin (2015)


Rauf, I.: Design and Validation of Stateful Composite RESTful Web Services. Ph.D. thesis, Turku Centre for Computer Science (2014)


[fig1]:./img/Ivanchikj2016-Fig1.png
[fig2]:./img/Ivanchikj2016-Fig2.png
[fig3]:./img/Ivanchikj2016-Fig3.png
[fig4]:./img/Ivanchikj2016-Fig4.png
[fig5]:./img/Ivanchikj2016-Fig5.png
[fig6]:./img/Ivanchikj2016-Fig6.png
