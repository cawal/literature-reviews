# How to review a paper?

Baseado em http://www.sciencemag.org/careers/2016/09/how-review-paper

## O que considerar quando decidindo se vai aceitar o convite a revisar um paper?

- Fatores:
    - Tenho conhecimento suficiente do assunto para dar uma opinião informada e útil?
    - Acho o tópico interessante?
    - estou livre de conflitos de interesse?
    - Tenho tempo disponível?

- Outros fatores:
    - Revista tem uma boa conduta científica?
    - O sistema de revisão é justo e cego?
    - Revistas de nicho costumam ter mais dificuldade em encontrar reviores.

## Abordagem para revisar o paper?

- Conheça os critérios e formatos usado pelo journal para a resposta do revisor;
- Pontos a considerar:
    - Está bem escrito?
    - A metodologia usada poderia responder a questão de pesquisa?
    - A literatura está bem selecionada e organizada?
    - A hipótese de pesquisa está clara? Ela faz sentido na situação atual?
    - O método está claro?
    - Há pontos em que os autores "fingiram que não viram" algo?
    - Os autores clamam resultados ou discussões que não são suportados pelos dados? Estão vendendo o peixe demais?
    - Como a pesquisa pode ser melhorada?
    - Estudos similares trazem resultados comparáveis?
- Formas de organizar os itens da revisão:
    - listas de Major items e minor items (falhas ou acertos)
    - Primeira leitura rápida, seguida de uma leitura de maior escrutínio;


## Como escrever a resposta?
- major e minor points;
- como os autores devem melhorar a pesquisa
- não ser julgador com os autores ("preguiçoso" ou "imcompetente" não são críticas a se fazer em um review), mas falar dos dados e da escrita de forma direcionada;
- citar razões fatuais e evidências nas críticas mais importantes;
- apresentar o que o paper clama de resultados
- sumário das qualidades e defeitos do paper;
- sugira revisões nas partes que a leitura foi difícil, ainda que o texto esteja provendo informaçẽops corretas;
- sugira revisões onde o texto apresenta informação errrada;


## Qual o tempo que leva revisar  um paper? 

De algumas horas a uns 2dias.

## dicas

- Revisão toma tempo, mas te leva a conhecer o que está na crista da onda de um campo de estudo;
- Paper deve ser válido e deve apresentar algo realmente novo, não apenas ser válido
- Se tiver vergonha de criticar o trabalho dos outros, finja que é o seu trabalho  e fale como ele deveria ser melhorado;
:wq

