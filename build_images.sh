#!/bin/bash

# must install mermaid.cli
# $ npm install -g mermaid.cli


#[ -x mmdc ] && echo "Command 'mmdc' not found. Use: npm install -g mermaid.cli" >&2 && exit 1


IMG_DIR="src/img/";
DPI=300;

for MMD in ./${IMG_DIR}*.mmd; do
    filename="$( basename "${MMD}" )"
    #extension="${filename##*.}"
    PNG="${IMG_DIR}${filename%.*}.png"

    if [ "${1}" = "-f" ] || [[ "${MMD}" -nt "${PNG}" ]] ; then
        echo "${MMD}" is newer than "${PNG}": Exporting
        mmdc --theme neutral -i "${MMD}" -o "${PNG}"
    else
        echo "${MMD}" is older than "${PNG}: Passing"
    fi
done


for PUML in $(find . -iname *.puml ) ./${IMG_DIR}*.puml; do
    filename="$( basename "${PUML}" )"
    PNG="${IMG_DIR}${filename%.*}.png"

    echo Exporting "${PUML}"
    java -jar bin/plantuml.jar -v -config plantuml.config  -tsvg "${PUML}"
done





for SVG  in ./${IMG_DIR}*.svg; do 
    filename="$( basename "${SVG}" )"
    #extension="${filename##*.}"
    PNG="${IMG_DIR}${filename%.*}.png"

    if [ "${1}" = "-f" ] || [[ "${SVG}" -nt "${PNG}" ]] ; then
        echo "${SVG}" is newer than "${PNG}": Exporting
        inkscape -D -z "--export-dpi=${DPI}" "--file=${SVG}" "--export-png=${PNG}"
    else
        echo "${SVG}" is older than "${PNG}: Passing"
    fi
done

 
