## Install environment

```bash
 sudo gem install jekyll-plantuml
 cp plantuml plantuml.config ~/bin
 cd ~/bin
 wget http://sourceforge.net/projects/plantuml/files/plantuml.jar/download -o plantuml.jar
```
